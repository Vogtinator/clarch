LDFLAGS += -g -lOpenCL -pthread
CFLAGS += -g -O3 -Wall -Wextra -pthread -Wno-unused-function -fno-strict-aliasing
CXXFLAGS += $(CFLAGS) -std=c++17

all: clarch clarch_cpu device-tree.dtb

CORE_OBJS += mmio.o interpreter.o mmu.o mmio.o cpu.o fp_interpreter.o

%.o: cpudefs.h fb_interpreter.h mmio.h core_globals.h mmu.h opencl_compat.h

clarch: main.o $(CORE_OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

clarch_cpu: main_cpu.o $(CORE_OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

clarch_cmp: main_cmp.o $(CORE_OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

%.dtb: device-tree.dts
	dtc -I dts -O dtb $^ > $@

clean:
	rm -f *.o clarch clarch_cpu

.PHONY: clean
