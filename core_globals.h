#pragma once

#include "cpudefs.h"
#include "mmu.h"

typedef struct core_globals {
	struct cpu cpu;
	unsigned int pe_id; // ID of this core
	addr_cache addr_cache;
    #if defined(__OPENCL_VERSION__) && __OPENCL_VERSION < 200
	    global uint8_t *memory;
    #endif
	global struct periph_global_state *periph_state;
	bool return_to_host; // If true, return from interpret
} core_globals;

#if !(defined(__OPENCL_VERSION__) && __OPENCL_VERSION < 200)
    extern global uint8_t *memory;
#endif

static uint8_t decode_rd(uint32_t instruction)
{
	return instruction & 0x1F;
}

static uint8_t decode_rn(uint32_t instruction)
{
	return (instruction >> 5) & 0x1F;
}

static uint8_t decode_rm(uint32_t instruction)
{
	return (instruction >> 16) & 0x1F;
}

static uint8_t decode_ra(uint32_t instruction)
{
	return (instruction >> 10) & 0x1F;
}

static uint8_t decode_rt(uint32_t instruction)
{
	return instruction & 0x1F;
}

static uint8_t decode_rt2(uint32_t instruction)
{
	return (instruction >> 10) & 0x1F;
}

static uint8_t decode_rs(uint32_t instruction)
{
	return (instruction >> 16) & 0x1F;
}

static void set_reg_sp(core_globals *globals, uint8_t reg, uint64_t value, bool sf)
{
	if(!sf)
		value = (uint32_t) value;

	globals->cpu.reg[reg] = value;
}

static uint64_t get_reg_sp(const core_globals *globals, uint8_t reg, bool sf)
{
	if(sf)
		return globals->cpu.reg[reg];
	else
		return (uint32_t) globals->cpu.reg[reg];
}

static void set_reg_wzr(core_globals *globals, uint8_t reg, uint64_t value, bool sf)
{
	if(reg == 31)
		return;

	set_reg_sp(globals, reg, value, sf);
}

static uint64_t get_reg_wzr(const core_globals *globals, uint8_t reg, bool sf)
{
	if(reg == 31)
		return 0;

	return get_reg_sp(globals, reg, sf);
}

uint64_t extended_register(uint64_t value, uint8_t option, uint8_t shift);
void period_step_cpu(struct core_globals *globals);
void handle_pending_interrupts(struct core_globals *globals);
void print_backtrace(core_globals *globals);
