#include "core_globals.h"

#include "mmio.h"

void dump_cpu_state(const struct cpu *cpu)
{
	for(int i = 0; i < 32; i += 2)
		printf("X%.02d: %016lx X%.02d: %016lx\r\n", i, cpu->reg[i], i + 1, cpu->reg[i+1]);

	printf("PC:  %016lx NZCV: %d%d%d%d\r\n", cpu->pc, cpu->nzcv.n, cpu->nzcv.z, cpu->nzcv.c, cpu->nzcv.v);

	// Floating point
	/*for(int i = 0; i < 32; i++)
		printf("V%.02d: %016lx %016lx (=%lf %lf)\r\n", i, cpu->v[i].l2[0], cpu->v[i].l2[1], cpu->v[i].d2[0], cpu->v[i].d2[1]);
*/
	// Chars
	for(int i = 0; i < 32; i++) {
		printf("V%.02d: %016lx %016lx (=", i, cpu->v[i].l2[0], cpu->v[i].l2[1]);
		for(int j = 0; j < 16; ++j)
			printf("%c", isprint(cpu->v[i].b16[j]) ? cpu->v[i].b16[j] : '?');
		printf(")\r\n");
	}
}

#ifndef __OPENCL_VERSION__
static unsigned char dump_bin[] = {
  0x60, 0x0c, 0x80, 0x92, 0x01, 0x02, 0x00, 0x10, 0x02, 0x00, 0x80, 0xd2,
  0x08, 0x07, 0x80, 0xd2, 0x01, 0x00, 0x00, 0xd4, 0x02, 0xe2, 0x84, 0xd2,
  0xe1, 0x63, 0x22, 0xcb, 0xe8, 0x07, 0x80, 0xd2, 0x01, 0x00, 0x00, 0xd4,
  0xe3, 0x03, 0x00, 0xaa, 0xe4, 0x03, 0x01, 0xaa, 0x20, 0x00, 0x80, 0xd2,
  0xe1, 0x03, 0x04, 0xaa, 0xe2, 0x03, 0x03, 0xaa, 0x08, 0x08, 0x80, 0xd2,
  0x01, 0x00, 0x00, 0xd4, 0x00, 0x00, 0x00, 0x14, 0x2f, 0x70, 0x72, 0x6f,
  0x63, 0x2f, 0x73, 0x65, 0x6c, 0x66, 0x2f, 0x6d, 0x61, 0x70, 0x73, 0x00
};

void dump_memmap(struct core_globals *globals)
{
	bool success = true;
	write_bytes(globals, globals->cpu.pc, dump_bin, sizeof(dump_bin), &success);
}
#endif

static uint32_t state_to_spsr(const struct cpu *cpu)
{
	return (unsigned) cpu->nzcv.n << 31 | cpu->nzcv.z << 30 | cpu->nzcv.c << 29 | cpu->nzcv.v << 28 |
	        cpu->DAIF | cpu->current_el << 2 | (!!cpu->current_el & cpu->SPSel);
}

static void spsr_to_state(struct core_globals *globals, uint32_t spsr)
{
	globals->cpu.nzcv = (struct NZCV){spsr & (1u<<31), spsr & (1<<30), spsr & (1<<29), spsr & (1<<28)};
	globals->cpu.DAIF = spsr & (0b1111 << 6);

	// Save active SP into SP_ELx
	globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0] = globals->cpu.reg[31];
	globals->cpu.SPSel = spsr & 1;
	uint8_t new_el = (spsr >> 2) & 3;
	if(new_el < globals->cpu.current_el)
		flush_addr_cache(globals);

	globals->cpu.current_el = new_el;
	// Load active SP from SP_ELx
	globals->cpu.reg[31] = globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0];
}

void handle_exception(struct core_globals *globals, uint64_t pc_ret, uint8_t target_el, uint32_t esr, uint32_t vect_offset)
{
	if(globals->cpu.current_el == target_el && globals->cpu.SPSel)
		vect_offset += 0x200;
	else if(globals->cpu.current_el == 0)
		vect_offset += 0x400;

	globals->cpu.SPSR_EL[target_el - 1] = state_to_spsr(&globals->cpu);
	globals->cpu.ELR_EL[target_el - 1] = pc_ret;
	globals->cpu.ESR_EL[target_el - 1] = esr;

	globals->cpu.DAIF = 0b1111 << 6;

	// Save active SP into SP_ELx
	globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0] = globals->cpu.reg[31];
	globals->cpu.SPSel = 1;
	if(target_el < globals->cpu.current_el)
		assert(false);

	globals->cpu.current_el = target_el;
	// Load active SP from SP_ELx
	globals->cpu.reg[31] = globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0];

	globals->cpu.pc = globals->cpu.VBAR_EL1 + vect_offset;
}

void handle_exception_return(struct core_globals *globals)
{
	globals->cpu.pc = globals->cpu.ELR_EL[globals->cpu.current_el - 1];
	spsr_to_state(globals, globals->cpu.SPSR_EL[globals->cpu.current_el - 1]);
}

// Assumes that FAR_ELx and DFSC in ESR_ELx are already set by translate_address
void handle_data_abort(struct core_globals *globals, bool write)
{
	uint32_t ESR = globals->cpu.ESR_EL[1-1];
	ESR |= (uint32_t)(0x24 + (globals->cpu.current_el == 1)) << 26; // EC
	ESR |= (1 << 25) | (0 << 24); // Invalid ISS, 32-bit instruction
	ESR |= write << 6; // WnR

	return handle_exception(globals, globals->cpu.pc, 1, ESR, 0x0);
}

// Assumes that FAR_ELx and DFSC in ESR_ELx are already set by translate_address
void handle_instruction_abort(core_globals *globals)
{
	uint32_t ESR = globals->cpu.ESR_EL[1-1];
	ESR |= (uint32_t)(0x20 + (globals->cpu.current_el == 1)) << 26; // EC
	ESR |= (1 << 25); // 32-bit instruction

	return handle_exception(globals, globals->cpu.pc, 1, ESR, 0x0);
}

__attribute__((unused)) void print_backtrace(core_globals *globals)
{
	global struct {
		uint64_t next, pc;
	} *frame = virt_ptr(globals, globals->cpu.reg[31], false);
	while(frame)
	{
		printf("%lx\n", frame->pc);
		frame = frame->next ? virt_ptr(globals, frame->next, false) : NULL;
	}
}

// Called every 8 clocks
void period_step_cpu(struct core_globals *globals)
{
	// Advance the timer
	globals->cpu.timer_counter += 1;

	// Compare physical time
	if(globals->cpu.timer_counter >= globals->cpu.timer_compare)
	{
		globals->cpu.CNTP_CTL_EL0 |= 1 << 2;
		bool trigger = (globals->cpu.CNTP_CTL_EL0 & 0b1) && !(globals->cpu.CNTP_CTL_EL0 & 0b10);
		gicv3_set_ppi(globals, 1, trigger);
	}
	else
		globals->cpu.CNTP_CTL_EL0 &= ~(1 << 2);

	// Compare virtual time
	if(globals->cpu.timer_counter - globals->cpu.timer_offset >= globals->cpu.timer_compare)
	{
		globals->cpu.CNTV_CTL_EL0 |= 1 << 2;
		bool trigger = (globals->cpu.CNTV_CTL_EL0 & 0b1) && !(globals->cpu.CNTV_CTL_EL0 & 0b10);
		gicv3_set_ppi(globals, 2, trigger);
	}
	else
		globals->cpu.CNTV_CTL_EL0 &= ~(1 << 2);
}

void handle_pending_interrupts(struct core_globals *globals)
{
	// Check whether FIQ is pending and not masked first
	if(globals->cpu.fiq_pending && !(globals->cpu.DAIF & (0b01000000)))
		return handle_exception(globals, globals->cpu.pc, 1, 0x2fu << 26, 0x100);

	// Only then check for IRQs
	if(globals->cpu.irq_pending && !(globals->cpu.DAIF & (0b10000000)))
		return handle_exception(globals, globals->cpu.pc, 1, 0x2fu << 26, 0x80);
}
