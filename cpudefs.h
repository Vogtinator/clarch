#pragma once

#include "opencl_compat.h"

#ifdef __cplusplus
extern "C" {
#endif

struct cpu {
	uint64_t reg[32]; // reg[31] is active SP
	union VReg {
		uchar16 b16;
		ushort8 h8;
		uint4 w4;
		ulong2 l2;
		float4 s4;
		double2 d2;
	} v[32];
	uint64_t pc;
	struct NZCV {
		bool n;
		bool z;
		bool c;
		bool v;
	} nzcv;
	uint32_t FPSR;
	uint32_t FPCR;
	uint64_t sp_el[2];
	uint32_t SPSR_EL[1];
	uint64_t ELR_EL[1];
	uint32_t ESR_EL[1];
	uint64_t FAR_EL[1];
	bool SPSel;
	uint8_t current_el;
	uint32_t SCTLR_EL1;
	uint64_t MAIR_EL1;
	uint64_t TCR_EL1;
	uint32_t CPACR_EL1;
	uint64_t TTBR0_EL1;
	uint64_t TTBR1_EL1;
	uint64_t VBAR_EL1;
	uint64_t TPIDRRO_EL0;
	uint64_t TPIDR_EL0;
	uint64_t TPIDR_EL1;
	uint32_t DAIF;
	bool irq_pending, fiq_pending;
	uint32_t CNTKCTL_EL1;
	uint32_t CNTFRQ_EL0;
	uint32_t CNTV_CTL_EL0;
	uint32_t CNTP_CTL_EL0;
	uint64_t timer_counter;
	uint64_t timer_offset;
	// virtual
	uint64_t timer_compare;
	uint32_t ICC_CTRLR_EL1;
	uint32_t ICC_PMR_EL1;
	uint32_t ICC_BPR1_EL1;
	uint32_t ICC_IGRPEN1_EL1;
	uint32_t ICC_IAR1_EL1;
};

enum ShiftType {
	LSL = 0,
	LSR = 1,
	ASR = 2,
	ROR = 3
};

enum Condition {
	CC_EQ=0, CC_NE,
	CC_CS, CC_CC,
	CC_MI, CC_PL,
	CC_VS, CC_VC,
	CC_HI, CC_LS,
	CC_GE, CC_LT,
	CC_GT, CC_LE,
	CC_AL, CC_NV
};

struct core_globals;

void dump_cpu_state(const struct cpu *cpu_state);
void handle_exception(struct core_globals *globals, uint64_t pc_ret, uint8_t target_el, uint32_t esr, uint32_t vect_offset);
void handle_exception_return(struct core_globals *globals);
void handle_data_abort(struct core_globals *globals, bool write);
void handle_instruction_abort(struct core_globals *globals);

struct periph_global_state;
kernel void interpret(global struct cpu *cpu_state, global struct periph_global_state *periph_state, global uint8_t *memory_ptr, unsigned int cycles
#ifndef __OPENCL_VERSION__
, unsigned int cpu_id
#endif
);
void init_cpu(struct cpu *spu_state);

#ifdef __cplusplus
}
#endif
