#include "fp_interpreter.h"

#define VECTOR_ELEM_COUNT(v) (sizeof(v) / sizeof(v[0]))

// Set all (if q: lower half) elements of v (treated as type) to expr
#define SET_VECTOR_ELEMS(v, type, q, expr) \
	do { \
	    for(unsigned i = 0; i < (VECTOR_ELEM_COUNT((v).type))/((q)?1:2); ++i) \
	        (v).type[i] = (expr); \
	} while(0)

// Clear the upper half of v
#define CLEAR_TOP_VECTOR_ELEMS(v) do { (v).l2[1] = 0; } while(0)

// Set each element in vd to expr. It can reference n and m.
#define OP_VECTOR(vd, vn, vm, type, q, expr) \
	do { \
	    for(unsigned i = 0; i < (VECTOR_ELEM_COUNT((vd).type))/((q)?1:2); ++i) \
        {\
	        typeof((vn).type[0]) n = (vn).type[i]; \
	        typeof((vm).type[0]) m = (vm).type[i]; \
	        (vd).type[i] = (expr); \
	    } \
	} while(0)

#define CMP_VECTOR(vd, vn, vm, type, member, q, expr) \
	do { \
	    for(unsigned i = 0; i < (VECTOR_ELEM_COUNT((vd).member))/((q)?1:2); ++i) \
        {\
	        type n = (vn).member[i]; \
	        type m = (vm).member[i]; \
	        (vd).member[i] = (expr) ? (type)~0ul : 0; \
	    } \
	} while(0)

#define OP_VECTOR_PAIR(vd, vn, vm, type, q, expr) \
	do { \
	    unsigned half_count = (VECTOR_ELEM_COUNT((vd).type))/((q)?2:4);\
	    for(unsigned i = 0; i < (VECTOR_ELEM_COUNT((vd).type))/((q)?1:2); ++i) \
        {\
	        typeof((vn).type[0]) n, m;\
	        n = i >= half_count ? (vm).type[(i - half_count)*2] : (vn).type[i*2]; \
	        m = i >= half_count ? (vm).type[(i - half_count)*2+1] : (vn).type[i*2+1]; \
	        (vd).type[i] = (expr); \
	    } \
	} while(0)

// The _BY_SCALE variants assign to b16/h8/w4/l2 depending on scale (log2 of size)
#define SET_VECTOR_ELEMS_BY_SCALE(v, scale, q, expr) \
	do { \
	    switch(scale) \
        { \
	    case 0: SET_VECTOR_ELEMS(v, b16, q, expr); break; \
	    case 1: SET_VECTOR_ELEMS(v, h8, q, expr); break; \
	    case 2: SET_VECTOR_ELEMS(v, w4, q, expr); break; \
	    case 3: SET_VECTOR_ELEMS(v, l2, q, expr); break; \
	    } \
	} while(0)

#define OP_VECTOR_BY_SCALE(vd, vn, vm, scale, q, expr) \
	do { \
	    switch(scale) \
        { \
	    case 0: OP_VECTOR(vd, vn, vm, b16, q, expr); break; \
	    case 1: OP_VECTOR(vd, vn, vm, h8, q, expr); break; \
	    case 2: OP_VECTOR(vd, vn, vm, w4, q, expr); break; \
	    case 3: OP_VECTOR(vd, vn, vm, l2, q, expr); break; \
	    } \
	} while(0)

// Sets the element to all-ones if expr true
#define CMP_VECTOR_BY_SCALE(vd, vn, vm, scale, u, q, expr) \
	do { \
	    if(u) \
	        switch(scale) \
            { \
	        case 0: CMP_VECTOR(vd, vn, vm, uint8_t, b16, q, expr); break; \
	        case 1: CMP_VECTOR(vd, vn, vm, uint16_t, h8, q, expr); break; \
	        case 2: CMP_VECTOR(vd, vn, vm, uint32_t, w4, q, expr); break; \
	        case 3: CMP_VECTOR(vd, vn, vm, uint64_t, l2, q, expr); break; \
	        } \
	    else \
	        switch(scale) \
            { \
	        case 0: CMP_VECTOR(vd, vn, vm, int8_t, b16, q, expr); break; \
	        case 1: CMP_VECTOR(vd, vn, vm, int16_t, h8, q, expr); break; \
	        case 2: CMP_VECTOR(vd, vn, vm, int32_t, w4, q, expr); break; \
	        case 3: CMP_VECTOR(vd, vn, vm, int64_t, l2, q, expr); break; \
	        } \
	} while(0)

#define OP_VECTOR_PAIR_BY_SCALE(vd, vn, vm, scale, q, expr) \
	do { \
	    switch(scale) \
        { \
	    case 0: OP_VECTOR_PAIR(vd, vn, vm, b16, q, expr); break; \
	    case 1: OP_VECTOR_PAIR(vd, vn, vm, h8, q, expr); break; \
	    case 2: OP_VECTOR_PAIR(vd, vn, vm, w4, q, expr); break; \
	    case 3: OP_VECTOR_PAIR(vd, vn, vm, l2, q, expr); break; \
	    } \
	} while(0)

// Sets NZCV based on the comparision of a and b
#define CMP_FLOAT(a, b) \
	do { \
	    if(a == b) globals->cpu.nzcv = (struct NZCV){false, true, true, false}; \
	    else if(a < b) globals->cpu.nzcv = (struct NZCV){true, false, false, false}; \
	    else globals->cpu.nzcv = (struct NZCV){false, false, true, false}; \
	} while(0)

// Returns the vector element width of the given encoded imm5 in log2(bytes)
static uint8_t decode_elem_scale(uint8_t imm5)
{
	return ctz32(imm5);
}

static uint8_t decode_elem_index(uint8_t imm5)
{
	return imm5 >> (ctz32(imm5) + 1);
}

static void zero_vreg(core_globals *globals, uint8_t reg)
{
	globals->cpu.v[reg].l2[0] = globals->cpu.v[reg].l2[1] = 0;
}

static uint32_t decode_vfp_imm_float(uint8_t imm)
{
	uint32_t sign = (uint32_t)(imm & 0x80) << 24;
	uint32_t exponent = ((~imm & 0x40) << 1 | (imm & 0x30) >> 4) << 23;
	if(imm & 0x40)
		exponent |= 0b11111 << 25;
	uint32_t frac = (imm & 0x0F) << 19;
	return sign | exponent | frac;
}

static uint64_t decode_vfp_imm_double(uint8_t imm)
{
	uint64_t sign = (uint64_t)(imm & 0x80) << 56;
	uint64_t exponent = (uint64_t)((~imm & 0x40) << 4 | (imm & 0x30) >> 4) << 52;
	if(imm & 0x40)
		exponent |= 0b11111111ull << 54;
	uint64_t frac = (uint64_t)(imm & 0x0F) << 48;
	return sign | exponent | frac;
}

static uint64_t decode_simd_imm(bool op, uint8_t cmode, uint8_t imm)
{
	switch(cmode >> 1)
	{
	case 0b000:
		return (uint64_t)imm << 32 | (uint64_t)imm << 0;
	case 0b001:
		return (uint64_t)imm << 40 | (uint64_t)imm << 8;
	case 0b010:
		return (uint64_t)imm << 48 | (uint64_t)imm << 16;
	case 0b011:
		return (uint64_t)imm << 56 | (uint64_t)imm << 24;
	case 0b100:
		return (uint64_t)imm << 48 | (uint64_t)imm << 32 | (uint64_t)imm << 16 | (uint64_t)imm << 0;
	case 0b101:
		return (uint64_t)imm << 56 | (uint64_t)imm << 40 | (uint64_t)imm << 24 | (uint64_t)imm << 8;
	case 0b110:
		if(cmode == 0b1100)
			return (uint64_t)imm << 40 | 0xFFull << 32 | (uint64_t)imm << 8 | 0xFFul;
		else // cmode == 0b1101
			return (uint64_t)imm << 48 | 0xFFFFull << 32 | (uint64_t)imm << 16 | 0xFFFFul;
	case 0b111:
		if(cmode == 0b1110 && !op)
			return imm * 0x0101010101010101ull;
		else if(cmode == 0b1110 && op)
		{
			return ((imm & 0b10000000) ? 0xFF00000000000000ull : 0)
			        | ((imm & 0b01000000) ? 0x00FF000000000000ull : 0)
			        | ((imm & 0b00100000) ? 0x0000FF0000000000ull : 0)
			        | ((imm & 0b00010000) ? 0x000000FF00000000ull : 0)
			        | ((imm & 0b00001000) ? 0x00000000FF000000ull : 0)
			        | ((imm & 0b00000100) ? 0x0000000000FF0000ull : 0)
			        | ((imm & 0b00000010) ? 0x000000000000FF00ull : 0)
			        | ((imm & 0b00000001) ? 0x00000000000000FFull : 0);
		}
		else if(cmode == 0b1111 && !op)
		{
			uint32_t h = (imm & 0x80u) << 24;
			h |= (uint32_t)~(imm & 0x40) << 24;
			h |= (imm & 0x40) ? 0x3E000000 : 0;
			h |= ((uint32_t)imm & 0x3F) << 19;

			return (uint64_t)h << 32 | h;
		}
		else // cmode == 0b1111 && op
		{
			uint32_t h = (imm & 0x80u) << 24;
			h |= (uint32_t)~(imm & 0x40) << 24;
			h |= (imm & 0x40) ? 0x3FC00000 : 0;
			h |= ((uint32_t)imm & 0x3F) << 16;
			return (uint64_t)h << 32;
		}
	}

	__builtin_unreachable();
}

bool interpret_fp_instruction(core_globals *globals, uint32_t instruction)
{
	uint8_t fpen = (globals->cpu.CPACR_EL1 >> 20) & 3;

	if((fpen & 1) == 0
	   || (fpen == 1 && globals->cpu.current_el == 0))
	{
		handle_exception(globals, globals->cpu.pc, 1, 0x07 << 26 | 1 << 25 | 0x1e << 20, 0);
		return true;
	}

	if((instruction & 0x3A000000) == 0x28000000) // Load/store register pair
	{
		bool load = instruction & (1 << 22),
		     writeback = instruction & (1 << 23),
		     postindex = !(instruction & (1 << 24)),
		     vector = instruction & (1 << 26);
		uint8_t opc = instruction >> 30;

		if(!vector)
			goto unimpl;

		// 0 = 32bit, 1 = 64bit, 2 = 128bit
		uint8_t scale = 2 + opc;

		if(!writeback && postindex) // No-allocate
			postindex = false;

		int32_t offset = (instruction & 0x003F8000) << 10; offset >>= 25;
		offset *= 1 << scale;

		uint64_t base = get_reg_sp(globals, decode_rn(instruction), true);
		if(!postindex)
			base += offset;

		bool success = true;
		ulong2 value, value2;
		switch(opc << 1 | load)
		{
		case 0b001: // LDP (32-bit)
			value[0] = read_word(globals, base, &success);
			if(success)
				value2[0] = read_word(globals, base + 4, &success);

			if(success)
			{
				zero_vreg(globals, decode_rt(instruction));
				zero_vreg(globals, decode_rt2(instruction));
				globals->cpu.v[decode_rt(instruction)].w4[0] = value[0];
				globals->cpu.v[decode_rt2(instruction)].w4[0] = value2[0];
			}
			break;
		case 0b011: // LDP (64-bit)
			value[0] = read_dword(globals, base, &success);
			if(success)
				value2[0] = read_dword(globals, base + 8, &success);

			if(success)
			{
				zero_vreg(globals, decode_rt(instruction));
				zero_vreg(globals, decode_rt2(instruction));
				globals->cpu.v[decode_rt(instruction)].l2[0] = value[0];
				globals->cpu.v[decode_rt2(instruction)].l2[0] = value2[0];
			}
			break;
		case 0b101: // LDP (128-bit)
			value[0] = read_dword(globals, base, &success);
			if(success)
				value[1] = read_dword(globals, base + 8, &success);
			if(success)
				value2[0] = read_dword(globals, base + 16, &success);
			if(success)
				value2[1] = read_dword(globals, base + 24, &success);

			if(success)
			{
				globals->cpu.v[decode_rt(instruction)].l2[0] = value[0];
				globals->cpu.v[decode_rt(instruction)].l2[1] = value[1];
				globals->cpu.v[decode_rt2(instruction)].l2[0] = value[0];
				globals->cpu.v[decode_rt2(instruction)].l2[1] = value2[1];
			}
			break;
		case 0b010: // STP (64-bit)
			value[0] = globals->cpu.v[decode_rt(instruction)].l2[0];
			value2[0] = globals->cpu.v[decode_rt2(instruction)].l2[0];

			write_dword(globals, base, value[0], &success);
			if(success)
				write_dword(globals, base + 8, value2[0], &success);
			break;
		case 0b100: // STP (128-bit)
			value[0] = globals->cpu.v[decode_rt(instruction)].l2[0];
			value[1] = globals->cpu.v[decode_rt(instruction)].l2[1];
			value2[0] = globals->cpu.v[decode_rt2(instruction)].l2[0];
			value2[1] = globals->cpu.v[decode_rt2(instruction)].l2[1];

			write_dword(globals, base, value[0], &success);
			if(success)
				write_dword(globals, base + 8, value[1], &success);
			if(success)
				write_dword(globals, base + 16, value2[0], &success);
			if(success)
				write_dword(globals, base + 24, value2[1], &success);
			break;
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, !load);
			return true;
		}

		if(writeback)
		{
			if(postindex)
				base += offset;

			set_reg_sp(globals, decode_rn(instruction), base, true);
		}
	}
	else if((instruction & 0x3E000000) == 0x3C000000) // Load/store register (SIMD)
	{
		bool writeback = false,
		     postindex = false,
		     vector = instruction & (1 << 26);

		uint8_t opc = (instruction >> 22) & 3,
		        size = instruction >> 30;
		int64_t offset = 0;

		if(vector)
			size |= (opc & 0b10) << 1;

		if(instruction & (1 << 24)) // Unsigned immediate
		{
			writeback = postindex = false;
			offset = (instruction >> 10) & 0xFFF;
			offset <<= size;
		}
		else if(instruction & (1 << 21)) // Register offset
		{
			uint8_t option = (instruction >> 13) & 0x7;
			bool s = instruction & (1 << 12);
			uint64_t rm = get_reg_wzr(globals, decode_rm(instruction), (option & 0b11) == 0b11);

			offset = extended_register(rm, option, s ? size : 0);
		}
		else
		{
			writeback = instruction & (1 << 10);
			postindex = ((instruction >> 10) & 0b11) == 0b01;

			if(((instruction >> 10) & 0b11) == 0b10) // Unprivileged
				goto unimpl;

			int16_t imm = (instruction & 0x001FF000) >> 5; imm >>= 7;
			offset = imm;
		}

		uint64_t base = get_reg_sp(globals, decode_rn(instruction), true);

		if(!postindex)
			base += offset;

		bool load = opc & 1;
		bool success = true;
		switch((size << 1) | load)
		{
		case 0b1000: // STR (SIMD/FP) (128-bit)
		{
			uint64_t value = globals->cpu.v[decode_rt(instruction)].l2[0];
			write_dword(globals, base, value, &success);
			if(success)
			{
				value = globals->cpu.v[decode_rt(instruction)].l2[1];
				write_dword(globals, base + 8, value, &success);
			}
			break;
		}
		case 0b1001: // LDR (SIMD/FP) (128-bit)
		{
			uint64_t value = read_dword(globals, base, &success),
			         value2;
			if(success)
				value2 = read_dword(globals, base + 8, &success);

			if(success)
			{
				globals->cpu.v[decode_rt(instruction)].l2[0] = value;
				globals->cpu.v[decode_rt(instruction)].l2[1] = value2;
			}
			break;
		}
		case 0b0111: // LDR (SIMD/FP) (64-bit)
		{
			uint64_t value = read_dword(globals, base, &success);

			if(success)
			{
				globals->cpu.v[decode_rt(instruction)].l2[0] = value;
				globals->cpu.v[decode_rt(instruction)].l2[1] = 0;
			}
			break;
		}
		case 0b0110: // STR (SIMD/FP) (64-bit)
		{
			uint64_t value = globals->cpu.v[decode_rt(instruction)].l2[0];
			write_dword(globals, base, value, &success);
			break;
		}
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, !load);
			return true;
		}
		else if(writeback)
		{
			if(postindex)
				base += offset;

			set_reg_sp(globals, decode_rn(instruction), base, true);
		}
	}
	else if((instruction & 0x3F000000) == 0x1C000000) // LDR (literal)
	{
		uint8_t opc = instruction >> 30;
		int32_t offset = (int32_t)(instruction << 8) >> 13;
		offset <<= 2;

		bool success = true;
		uint64_t address = globals->cpu.pc + offset;
		uint64_t value[2] = {0, 0};

		switch(opc)
		{
		case 0b00:
			value[0] = read_word(globals, address, &success);
			break;
		case 0b01:
			value[0] = read_dword(globals, address, &success);
			break;
		case 0b10:
			value[0] = read_dword(globals, address, &success);
			if(success)
				value[1] = read_dword(globals, address + 8, &success);
			break;
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, false);
			return true;
		}

		globals->cpu.v[decode_rt(instruction)].l2[0] = value[0];
		globals->cpu.v[decode_rt(instruction)].l2[1] = value[1];
	}
	else if((instruction & 0xBFE0FC00) == 0x0E000C00) // DUP (general)
	{
		bool q = instruction & (1 << 30);
		uint8_t imm5 = (instruction >> 16) & 0x1F;

		uint8_t scale = decode_elem_scale(imm5);
		uint64_t value = get_reg_wzr(globals, decode_rn(instruction), true);

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		SET_VECTOR_ELEMS_BY_SCALE(vd, scale, q, value);

		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(vd);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0x9F20FC00) == 0x0E208400) // ADD/SUB (vector)
	{
		bool q = instruction & (1 << 30),
		     u = instruction & (1 << 29);

		uint8_t scale = (instruction >> 22) & 3;

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		if(u) // SUB
			OP_VECTOR_BY_SCALE(vd,
			                   globals->cpu.v[decode_rn(instruction)],
			                   globals->cpu.v[decode_rm(instruction)],
			                   scale, q,
			                   n - m);
		else // ADD
			OP_VECTOR_BY_SCALE(vd,
			                   globals->cpu.v[decode_rn(instruction)],
			                   globals->cpu.v[decode_rm(instruction)],
			                   scale, q,
			                   n + m);


		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0xBF202000) == 0x0C002000) // LD1/ST1 (multiple structures)
	{
		bool l = instruction & (1 << 22),
		     postindex = instruction & (1 << 23),
		     q = instruction & (1 << 30);

		uint8_t opc = (instruction >> 12) & 0xF,
		        size = (instruction >> 10) & 3;

		if(!postindex && decode_rm(instruction) != 0)
			goto unimpl;

		uint8_t ebytes = 1 << size,
		        elements = 16 >> (size + 1 - q);

		uint8_t rpt, selem;

		switch(opc)
		{
		case 0b0000: rpt = 1; selem = 4; break; // LD4/ST4 (4 reg)
		case 0b0010: rpt = 4; selem = 1; break; // LD1/ST1 (4 reg)
		case 0b0100: rpt = 1; selem = 3; break; // LD3/ST3 (3 reg)
		case 0b0110: rpt = 3; selem = 1; break; // LD1/ST1 (3 reg)
		case 0b0111: rpt = 1; selem = 1; break; // LD1/ST1 (1 reg)
		case 0b1000: rpt = 1; selem = 2; break; // LD2/ST2 (2 reg)
		case 0b1010: rpt = 2; selem = 1; break; // LD1/ST1 (2 reg)
		default:
			goto unimpl;
		}

		if(size == 3 && !q && selem != 1)
			goto unimpl;

		uint64_t address = get_reg_sp(globals, decode_rn(instruction), true),
		         offset = 0;

		if(l)
			for(int r = 0; r < rpt; ++r)
				zero_vreg(globals, (decode_rt(instruction) + r) & 31);

		for(uint8_t r = 0; r < rpt; ++r)
			for(uint8_t e = 0; e < elements; ++e)
			{
				uint8_t treg = decode_rt(instruction) + r;
				for(uint8_t s = 0; s < selem; ++s)
				{
					treg = treg & 31;

					bool success = true;
					if(l)
						read_bytes(globals, address + offset, globals->cpu.v[treg].b16 + (e << size), ebytes, &success);
					else
						write_bytes(globals, address + offset, globals->cpu.v[treg].b16 + (e << size), ebytes, &success);

					if(!success)
					{
						handle_data_abort(globals, !l);
						return true;
					}

					if(!q && l)
						CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[treg]);

					offset += ebytes;
					treg++;
				}
			}

		if(postindex)
		{
			if(decode_rm(instruction) != 31)
				offset = get_reg_wzr(globals, decode_rm(instruction), true);
			set_reg_sp(globals, decode_rn(instruction), address + offset, true);
		}
	}
	else if((instruction & 0x9F3FEC00) == 0x0E208800) // CMGT/CMGE/CMEQ/CMLE (Zero)
	{
		bool q = instruction & (1 << 30),
		     u = instruction & (1 << 29),
		     op = instruction & (1 << 12);

		uint8_t scale = (instruction >> 22) & 3;

		union VReg zero_reg;
		zero_reg.l2[0] = zero_reg.l2[1] = 0;

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		switch(op << 1 | op)
		{
		case 0b00: // CMGT
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    zero_reg,
			                    scale, u, q,
			                    n > m);
			break;
		case 0b01: // CMGE
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    zero_reg,
			                    scale, u, q,
			                    n >= m);
			break;
		case 0b10: // CMEQ
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    zero_reg,
			                    scale, u, q,
			                    n == m);
			break;
		case 0b11: // CMGT
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    zero_reg,
			                    scale, u, q,
			                    n <= m);
			break;
		}

		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(vd);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0x9F20FC00) == 0x0E208C00) // CMEQ (register)
	{
		bool q = instruction & (1 << 30),
		     u = instruction & (1 << 29);

		uint8_t scale = (instruction >> 22) & 3;

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		if(u)
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    globals->cpu.v[decode_rm(instruction)],
			                    scale, u, q,
			                    n == m);
		else
			CMP_VECTOR_BY_SCALE(vd,
			                    globals->cpu.v[decode_rn(instruction)],
			                    globals->cpu.v[decode_rm(instruction)],
			                    scale, u, q,
			                    n & m);

		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(vd);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0xBF20FC00) == 0x0E201C00) // AND/ORR (vector)
	{
		bool q = instruction & (1 << 30);

		uint8_t size = instruction >> 22 & 3;

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		switch(size)
		{
		case 0b00: // AND
			OP_VECTOR(vd,
			          globals->cpu.v[decode_rn(instruction)],
			          globals->cpu.v[decode_rm(instruction)],
			          b16, q,
			          n & m);
			break;
		case 0b01:
			OP_VECTOR(vd,
			          globals->cpu.v[decode_rn(instruction)],
			          globals->cpu.v[decode_rm(instruction)],
			          b16, q,
			          n & ~m);
			break;
		case 0b10: // ORR
			OP_VECTOR(vd,
			          globals->cpu.v[decode_rn(instruction)],
			          globals->cpu.v[decode_rm(instruction)],
			          b16, q,
			          n | m);
			break;
		case 0b11:
			OP_VECTOR(vd,
			          globals->cpu.v[decode_rn(instruction)],
			          globals->cpu.v[decode_rm(instruction)],
			          b16, q,
			          n | ~m);
			break;
		}

		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(vd);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0xBF20FC00) == 0x0E20BC00) // ADDP
	{
		bool q = instruction & (1 << 30);

		uint8_t scale = (instruction >> 22) & 3;

		union VReg vd = globals->cpu.v[decode_rd(instruction)];

		OP_VECTOR_PAIR_BY_SCALE(vd,
		                        globals->cpu.v[decode_rn(instruction)],
		                        globals->cpu.v[decode_rm(instruction)],
		                        scale, q,
		                        n + m);

		if(!q)
			CLEAR_TOP_VECTOR_ELEMS(vd);

		globals->cpu.v[decode_rd(instruction)] = vd;
	}
	else if((instruction & 0xBFE0FC00) == 0x0E003C00) // UMOV to general
	{
		bool q = instruction & (1 << 30);
		uint8_t imm5 = (instruction >> 16) & 0x1F;

		uint64_t value;

		switch(decode_elem_scale(imm5))
		{
		case 0:
			value =  globals->cpu.v[decode_rn(instruction)].b16[decode_elem_index(imm5)];
			break;
		case 1:
			value =  globals->cpu.v[decode_rn(instruction)].h8[decode_elem_index(imm5)];
			break;
		case 2:
			value =  globals->cpu.v[decode_rn(instruction)].w4[decode_elem_index(imm5)];
			break;
		case 3:
			if(!q) goto unimpl;
			value = globals->cpu.v[decode_rn(instruction)].l2[decode_elem_index(imm5)];
			break;
		default:
			goto unimpl;
		}

		set_reg_wzr(globals, decode_rd(instruction), value, true);
	}
	else if((instruction & 0x7F36FC00) == 0x1E260000) // FMOV (general)
	{
		bool sf = instruction & (1u << 31);
		uint8_t type = (instruction >> 22) & 3,
		        rmode = (instruction >> 19) & 3,
		        opcode = (instruction >> 16) & 7;

		if((type == 0b10 && (rmode != 0b01 || (opcode >> 1) != 0b11))
		   || type == 0b11)
			goto unimpl;

		switch(opcode << 2 | rmode)
		{
		case 0b11000: // FtoI
			if(!sf && type == 0b00)
				set_reg_wzr(globals, decode_rd(instruction), globals->cpu.v[decode_rn(instruction)].w4[0], sf);
			else if(sf && type == 0b01)
				set_reg_wzr(globals, decode_rd(instruction), globals->cpu.v[decode_rn(instruction)].l2[0], sf);
			else
				goto unimpl;
			break;
		case 0b11100: // ItoF
			if(!sf && type == 0b00)
				globals->cpu.v[decode_rd(instruction)].w4[0] = get_reg_wzr(globals, decode_rn(instruction), sf);
			else if(sf && type == 0b01)
				globals->cpu.v[decode_rd(instruction)].l2[0] = get_reg_wzr(globals, decode_rn(instruction), sf);
			else
				goto unimpl;
			break;
		case 0b11001: // FtoI d[1]
			set_reg_wzr(globals, decode_rd(instruction), globals->cpu.v[decode_rn(instruction)].l2[1], sf);
			break;
		case 0b11101: // ItoF d[1]
			globals->cpu.v[decode_rd(instruction)].l2[1] = get_reg_wzr(globals, decode_rn(instruction), sf);
			break;
		default:
			goto unimpl;
		}
	}
	else if((instruction & 0xFFA01FE0) == 0x1E201000) // FMOV (scalar, immediate)
	{
		bool type = instruction & (1 << 22);
		uint8_t imm = instruction >> 13;

		if(type) // 64-bit
		{
			globals->cpu.v[decode_rd(instruction)].l2[0] = decode_vfp_imm_double(imm);
			CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
		}
		else // 32-bit
		{
			globals->cpu.v[decode_rd(instruction)].l2[0] = decode_vfp_imm_float(imm);
			CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
		}
	}
	else if((instruction & 0x7FBEFC00) == 0x1E220000) // [SU]CVTF (scalar, integer)
	{
		bool sf = instruction & (1u << 31),
		     type = instruction & (1 << 22),
		     nosign = instruction & (1 << 16);

		zero_vreg(globals, decode_rd(instruction));

		switch(sf << 2 | type << 1 | nosign)
		{
		case 0b000: // signed 32-bit to float
			globals->cpu.v[decode_rd(instruction)].s4[0] = (int32_t)get_reg_wzr(globals, decode_rn(instruction), false);
			break;
		case 0b001: // unsigned 32-bit to float
			globals->cpu.v[decode_rd(instruction)].s4[0] = (uint32_t)get_reg_wzr(globals, decode_rn(instruction), false);
			break;
		case 0b010: // signed 32-bit to double
			globals->cpu.v[decode_rd(instruction)].d2[0] = (int32_t)get_reg_wzr(globals, decode_rn(instruction), false);
			break;
		case 0b011: // unsigned 32-bit to double
			globals->cpu.v[decode_rd(instruction)].d2[0] = (uint32_t)get_reg_wzr(globals, decode_rn(instruction), false);
			break;
		case 0b100: // signed 64-bit to float
			globals->cpu.v[decode_rd(instruction)].s4[0] = (int64_t)get_reg_wzr(globals, decode_rn(instruction), true);
			break;
		case 0b101: // unsigned 64-bit to float
			globals->cpu.v[decode_rd(instruction)].s4[0] = (uint64_t)get_reg_wzr(globals, decode_rn(instruction), true);
			break;
		case 0b110: // signed 64-bit to double
			globals->cpu.v[decode_rd(instruction)].d2[0] = (int64_t)get_reg_wzr(globals, decode_rn(instruction), true);
			break;
		case 0b111: // signed 64-bit to double
			globals->cpu.v[decode_rd(instruction)].d2[0] = (uint64_t)get_reg_wzr(globals, decode_rn(instruction), true);
			break;
		}
	}
	else if((instruction & 0x7FBEFC00) == 0x1E380000) // FCVT[NPMZ][SU]
	{
		bool sf = instruction & (1u << 31),
		     type = instruction & (1 << 22),
		     nosign = instruction & (1 << 16);

		uint8_t rmode = (instruction >> 19) & 0b11;

		// TODO: We don't care about rounding modes here
		(void) rmode;

		switch(sf << 2 | type << 1 | nosign)
		{
		case 0b000: // float to signed 32-bit
			set_reg_wzr(globals, decode_rd(instruction), (int32_t)globals->cpu.v[decode_rn(instruction)].s4[0], false);
			break;
		case 0b001: // float to unsigned 32-bit
			set_reg_wzr(globals, decode_rd(instruction), (uint32_t)globals->cpu.v[decode_rn(instruction)].s4[0], false);
			break;
		case 0b010: // double to signed 32-bit
			set_reg_wzr(globals, decode_rd(instruction), (int32_t)globals->cpu.v[decode_rn(instruction)].d2[0], false);
			break;
		case 0b011: // double to unsigned 32-bit
			set_reg_wzr(globals, decode_rd(instruction), (uint32_t)globals->cpu.v[decode_rn(instruction)].d2[0], false);
			break;
		case 0b100: // float to signed 64-bit
			set_reg_wzr(globals, decode_rd(instruction), (int64_t)globals->cpu.v[decode_rn(instruction)].s4[0], true);
			break;
		case 0b101: // float to unsigned 64-bit
			set_reg_wzr(globals, decode_rd(instruction), (uint64_t)globals->cpu.v[decode_rn(instruction)].s4[0], true);
			break;
		case 0b110: // double to signed 64-bit
			set_reg_wzr(globals, decode_rd(instruction), (int64_t)globals->cpu.v[decode_rn(instruction)].d2[0], true);
			break;
		case 0b111: // double to unsigned 64-bit
			set_reg_wzr(globals, decode_rd(instruction), (uint64_t)globals->cpu.v[decode_rn(instruction)].d2[0], true);
			break;
		}
	}
	else if((instruction & 0xFFA0EC00) == 0x1E202800) // FADD/FSUB (scalar)
	{
		bool type = instruction & (1 << 22),
		     op = instruction & (1 << 12);

		switch(type << 1 | op)
		{
		case 0b00: // float + float
			globals->cpu.v[decode_rd(instruction)].s4[0] = globals->cpu.v[decode_rn(instruction)].s4[0] + globals->cpu.v[decode_rm(instruction)].s4[0];
			globals->cpu.v[decode_rd(instruction)].s4[1] = 0;
			break;
		case 0b01: // float - float
			globals->cpu.v[decode_rd(instruction)].s4[0] = globals->cpu.v[decode_rn(instruction)].s4[0] - globals->cpu.v[decode_rm(instruction)].s4[0];
			globals->cpu.v[decode_rd(instruction)].s4[1] = 0;
			break;
		case 0b10: // double + double
			globals->cpu.v[decode_rd(instruction)].d2[0] = globals->cpu.v[decode_rn(instruction)].d2[0] + globals->cpu.v[decode_rm(instruction)].d2[0];
			break;
		case 0b11: // double - double
			globals->cpu.v[decode_rd(instruction)].d2[0] = globals->cpu.v[decode_rn(instruction)].d2[0] - globals->cpu.v[decode_rm(instruction)].d2[0];
			break;
		}

		CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
	}
	else if((instruction & 0xFFA0FC07) == 0x1E202000) // FCMP(E)
	{
		bool datasize = instruction & (1 << 22),
		     signal_nans = instruction & (1 << 4),
		     cmp_zero = instruction & (1 << 3);

		if(!datasize)
		{
			float a = globals->cpu.v[decode_rn(instruction)].s4[0],
			      b = 0.0f;

			if(!cmp_zero)
				b = globals->cpu.v[decode_rm(instruction)].s4[0];

			if(signal_nans && (isnan(a) || isnan(b)))
				goto unimpl;

			CMP_FLOAT(a, b);
		}
		else
		{
			double a = globals->cpu.v[decode_rn(instruction)].d2[0],
			       b = 0.0f;

			if(!cmp_zero)
				b = globals->cpu.v[decode_rm(instruction)].d2[0];

			if(signal_nans && (isnan(a) || isnan(b)))
				goto unimpl;

			CMP_FLOAT(a, b);
		}
	}
	else if((instruction & 0xFFA07C00) == 0x1E200800) // F(N)MUL
	{
		bool datasize = instruction & (1 << 22),
		     negate = instruction & (1 << 15);

		if(!datasize)
		{
			float a = globals->cpu.v[decode_rn(instruction)].s4[0],
			      b = globals->cpu.v[decode_rm(instruction)].s4[0];

			float res = a * b;

			if(negate)
				res = -res;

			globals->cpu.v[decode_rd(instruction)].s4[0] = res;
			globals->cpu.v[decode_rd(instruction)].s4[1] = 0;
		}
		else
		{
			double a = globals->cpu.v[decode_rn(instruction)].d2[0],
			       b = globals->cpu.v[decode_rm(instruction)].d2[0];

			double res = a * b;

			if(negate)
				res = -res;

			globals->cpu.v[decode_rd(instruction)].d2[0] = res;
		}

		CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
	}
	else if((instruction & 0xFFA08000) == 0x1F000000) // F(N)MADD, F(N)MSUB
	{
		bool datasize = instruction & (1 << 22),
		     o1 = instruction & (1 << 21),
		     o0 = instruction & (1 << 15);

		if(!datasize)
		{
			float a = globals->cpu.v[decode_ra(instruction)].s4[0],
			      n = globals->cpu.v[decode_rn(instruction)].s4[0],
			      m = globals->cpu.v[decode_rm(instruction)].s4[0];

			if(o1)
				a = -a;

			if(o0 != o1)
				n = -n;

			globals->cpu.v[decode_rd(instruction)].s4[0] = a + n * m;
			globals->cpu.v[decode_rd(instruction)].s4[1] = 0;
		}
		else
		{
			double a = globals->cpu.v[decode_ra(instruction)].d2[0],
			       n = globals->cpu.v[decode_rn(instruction)].d2[0],
			       m = globals->cpu.v[decode_rm(instruction)].d2[0];

			if(o1)
				a = -a;

			if(o0 != o1)
				n = -n;

			globals->cpu.v[decode_rd(instruction)].d2[0] = a + n * m;
		}

		CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
	}
	else if((instruction & 0xFFBFFC00) == 0x1E204000) // FMOV (register)
	{
		bool sf = instruction & (1 << 22);

		if(!sf)
		{
			globals->cpu.v[decode_rd(instruction)].w4[0] = globals->cpu.v[decode_rn(instruction)].w4[0];
			globals->cpu.v[decode_rd(instruction)].w4[1] = 0;
		}
		else
			globals->cpu.v[decode_rd(instruction)].l2[0] = globals->cpu.v[decode_rn(instruction)].l2[0];

		CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
	}
	else if((instruction & 0x9FF80C00) == 0x0F000400) // MOVI
	{
		bool op = instruction & (1 << 29),
		     q = instruction & (1 << 30);

		uint8_t imm = ((instruction >> 11) & 0xE0)
		              | ((instruction >> 5) & 0x1F),
		        cmode = (instruction >> 12) & 0xF;

		uint64_t imm64 = decode_simd_imm(op, cmode, imm);

		if(cmode == 0b1111 && op && !q)
			goto unimpl;

		enum MOVI_TYPE {
			MOVI_OP_MOVI,
			MOVI_OP_MVNI,
			MOVI_OP_ORR,
			MOVI_OP_BIC
		};

		enum MOVI_TYPE types[32] = {MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_ORR,  MOVI_OP_BIC,
			                        MOVI_OP_MOVI, MOVI_OP_MVNI, MOVI_OP_MOVI, MOVI_OP_MVNI,
			                        MOVI_OP_MOVI, MOVI_OP_MOVI, MOVI_OP_MOVI, MOVI_OP_MOVI};

		switch(types[cmode << 1 | op])
		{
		case MOVI_OP_MOVI:
			globals->cpu.v[decode_rd(instruction)].l2[0] = imm64;
			if(q)
				globals->cpu.v[decode_rd(instruction)].l2[1] = imm64;
			else
				globals->cpu.v[decode_rd(instruction)].l2[1] = 0;
			break;
		case MOVI_OP_MVNI:
			globals->cpu.v[decode_rd(instruction)].l2[0] = ~imm64;
			if(q)
				globals->cpu.v[decode_rd(instruction)].l2[1] = ~imm64;
			else
				globals->cpu.v[decode_rd(instruction)].l2[1] = 0;
			break;
		case MOVI_OP_ORR:
			globals->cpu.v[decode_rd(instruction)].l2[0] |= imm64;
			if(q)
				globals->cpu.v[decode_rd(instruction)].l2[1] |= imm64;
			else
				globals->cpu.v[decode_rd(instruction)].l2[1] = 0;
			break;
		case MOVI_OP_BIC:
			globals->cpu.v[decode_rd(instruction)].l2[0] &= ~imm64;
			if(q)
				globals->cpu.v[decode_rd(instruction)].l2[1] &= ~imm64;
			else
				globals->cpu.v[decode_rd(instruction)].l2[1] = 0;
			break;
		}
	}
	else if((instruction & 0xFFA0FC00) == 0x1E201800) // FDIV
	{
		bool type = instruction & (1 << 22);

		if(!type)
		{
			globals->cpu.v[decode_rd(instruction)].s4[0] = globals->cpu.v[decode_rn(instruction)].s4[0] / globals->cpu.v[decode_rm(instruction)].s4[0];
			globals->cpu.v[decode_rd(instruction)].s4[1] = 0;
		}
		else
			globals->cpu.v[decode_rd(instruction)].d2[0] = globals->cpu.v[decode_rn(instruction)].d2[0] / globals->cpu.v[decode_rm(instruction)].d2[0];

		CLEAR_TOP_VECTOR_ELEMS(globals->cpu.v[decode_rd(instruction)]);
	}
	else
	{
        unimpl:
		return false;
	}

	globals->cpu.pc += 4;
	return true;
}
