#pragma once

#include "core_globals.h"

#ifdef __cplusplus
extern "C" {
#endif

bool interpret_fp_instruction(struct core_globals *globals, uint32_t instruction);

#ifdef __cplusplus
}
#endif
