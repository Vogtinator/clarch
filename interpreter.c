#include "core_globals.h"
#include "fp_interpreter.h"
#include "mmio.h"

#ifndef __OPENCL_VERSION__
#include <memory.h>
#endif

static uint64_t shifted_register(const core_globals *globals, uint32_t instruction, bool sf)
{
	uint64_t rm = get_reg_wzr(globals, decode_rm(instruction), sf);
	uint8_t imm6 = (instruction >> 10) & 0x3f;

	if(!sf)
		imm6 &= 0x1F;

	enum ShiftType shift = (instruction >> 22) & 3;
	switch(shift)
	{
	case LSL:
		if(sf)
			return rm << imm6;
		else
			return (uint32_t)(rm << imm6);
	case LSR:
		return rm >> imm6;
	case ASR:
		if(sf)
			return (int64_t)(rm) >> imm6;
		else
			return (int32_t)(rm) >> imm6;
	case ROR:
		if(!imm6)
			return rm;

		if(sf)
			return (rm >> imm6) | (rm << (64 - imm6));
		else
			return (rm >> imm6) | (uint32_t)(rm << (32 - imm6));
	}

	__builtin_unreachable();
}

static uint64_t shifted_immediate(uint32_t instruction)
{
	unsigned shift = (instruction >> 22) & 3;
	return (uint64_t)((instruction >> 10) & 0xFFF) << (shift*12);
}

uint64_t extended_register(uint64_t value, uint8_t option, uint8_t shift)
{
	switch (option)
	{
	case 0b000: // UXTB
		value = (uint8_t) value;
		break;
	case 0b001: // UXTH
		value = (uint16_t) value;
		break;
	case 0b010: // UXTW
		value = (uint32_t) value;
		break;
	case 0b011: // UXTX
		value = (uint64_t) value;
		break;
	case 0b100: // SXTB
		value = (int64_t) (int8_t) value;
		break;
	case 0b101: // SXTH
		value = (int64_t) (int16_t) value;
		break;
	case 0b110: // SXTW
		value = (int64_t) (int32_t) value;
		break;
	case 0b111: // SXTX
		value = (int64_t) value;
	}

	return value << shift;
}

// Copied from QEMU as nobody should ever implement this fucking crap again
static uint64_t bitfield_replicate(uint64_t mask, unsigned int e)
{
	while (e < 64) {
		mask |= mask << e;
		e *= 2;
	}
	return mask;
}

/* Return a value with the bottom len bits set (where 0 < len <= 64) */
static inline uint64_t bitmask64(unsigned int length)
{
	// ~0UL >> (64 - length) does not work correctly, compiler bug.
	return ~(~1ul << (length-1));
}

// Returns false if invalid encoding
static bool logical_immediate(uint32_t instruction, uint64_t *result)
{
	uint32_t immr = (instruction >> 16) & 0x3F,
	         imms = (instruction >> 10) & 0x3F,
	         immn = (instruction >> 22) & 1;

	uint64_t mask;
	unsigned e, levels, s, r;
	int len;

	/* The bit patterns we create here are 64 bit patterns which
	 * are vectors of identical elements of size e = 2, 4, 8, 16, 32 or
	 * 64 bits each. Each element contains the same value: a run
	 * of between 1 and e-1 non-zero bits, rotated within the
	 * element by between 0 and e-1 bits.
	 *
	 * The element size and run length are encoded into immn (1 bit)
	 * and imms (6 bits) as follows:
	 * 64 bit elements: immn = 1, imms = <length of run - 1>
	 * 32 bit elements: immn = 0, imms = 0 : <length of run - 1>
	 * 16 bit elements: immn = 0, imms = 10 : <length of run - 1>
	 *  8 bit elements: immn = 0, imms = 110 : <length of run - 1>
	 *  4 bit elements: immn = 0, imms = 1110 : <length of run - 1>
	 *  2 bit elements: immn = 0, imms = 11110 : <length of run - 1>
	 * Notice that immn = 0, imms = 11111x is the only combination
	 * not covered by one of the above options; this is reserved.
	 * Further, <length of run - 1> all-ones is a reserved pattern.
	 *
	 * In all cases the rotation is by immr % e (and immr is 6 bits).
	 */

	/* First determine the element size */
	len = 31 - clz32((immn << 6) | (~imms & 0x3f));
	if (len < 1) {
		/* This is the immn == 0, imms == 0x11111x case */
		return false;
	}
	e = 1 << len;

	levels = e - 1;
	s = imms & levels;
	r = immr & levels;

	if (s == levels) {
		/* <length of run - 1> mustn't be all-ones. */
		return false;
	}

	/* Create the value of one element: s+1 set bits rotated
	 * by r within the element (which is e bits wide)...
	 */
	mask = bitmask64(s + 1);
	if (r) {
		mask = (mask >> r) | (mask << (e - r));
		mask &= bitmask64(e);
	}
	/* ...then replicate the element over the whole 64 bit value */
	mask = bitfield_replicate(mask, e);
	*result = mask;
	return true;
}

static void adjust_nzcv_value(core_globals *globals, uint64_t value, bool sf)
{
	bool n = false;

	if(sf)
		n = value & (1ul << 63);
	else
		n = value & (1u << 31);

	globals->cpu.nzcv = (struct NZCV){n, value == 0, false, false};
}

static bool test_condition(const core_globals *globals, enum Condition cond)
{
	bool exec;

	switch(cond)
	{
	    case CC_AL: case CC_NV: return true; break;
	    case CC_EQ: case CC_NE: exec = globals->cpu.nzcv.z; break;
	    case CC_CS: case CC_CC: exec = globals->cpu.nzcv.c; break;
	    case CC_MI: case CC_PL: exec = globals->cpu.nzcv.n; break;
	    case CC_VS: case CC_VC: exec = globals->cpu.nzcv.v; break;
	    case CC_HI: case CC_LS: exec = !globals->cpu.nzcv.z && globals->cpu.nzcv.c; break;
	    case CC_GE: case CC_LT: exec = globals->cpu.nzcv.n == globals->cpu.nzcv.v; break;
	    case CC_GT: case CC_LE: exec = !globals->cpu.nzcv.z && globals->cpu.nzcv.n == globals->cpu.nzcv.v; break;
	    default: __builtin_unreachable();
	}

	return exec ^ (cond & 1);
}

static uint64_t add_setcc(core_globals *globals, uint64_t left, uint64_t right, bool carry, bool sf)
{
	if(sf)
	{
		uint64_t sum = left + right + carry;

		globals->cpu.nzcv.z = sum == 0;
		globals->cpu.nzcv.n = sum >> 63;
		globals->cpu.nzcv.c = (sum == left) ? carry : (sum < left);
		globals->cpu.nzcv.v = (int64_t)((left ^ sum) & (right ^ sum)) < 0;
		return sum;
	}
	else
	{
		uint32_t sum = left + right + carry;

		globals->cpu.nzcv.z = sum == 0;
		globals->cpu.nzcv.n = sum >> 31;
		globals->cpu.nzcv.c = (sum == (uint32_t)left) ? carry : (sum < (uint32_t)left);
		globals->cpu.nzcv.v = (int32_t)(((uint32_t)(left) ^ sum) & ((uint32_t)(right) ^ sum)) < 0;

		return sum;
	}
}

static uint16_t rev16(uint16_t src)
{
	return (src & 0xFF00) >> 8 | (src & 0x00FF) << 8;
}

static uint32_t rev32(uint32_t src)
{
	return rev16(src >> 16) | (uint32_t)rev16(src) << 16;
}

static uint64_t rev64(uint64_t src)
{
	return rev32(src >> 32) | (uint64_t)rev32(src) << 32;
}

constant static const unsigned char BitReverseTable256[256] =
{
#   define R2(n)     n,     n + 2*64,     n + 1*64,     n + 3*64
#   define R4(n) R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
#   define R6(n) R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )
    R6(0), R6(2), R6(1), R6(3)
};

static uint8_t revbits(uint8_t src)
{
	// From https://graphics.stanford.edu/~seander/bithacks.html#BitReverseTable
	return BitReverseTable256[src];
}

static bool interpret_instruction(core_globals *globals, uint32_t instruction)
{
	if((instruction & 0x7C000000) == 0x14000000) // Unconditional branch (immediate)
	{
		if(instruction & (1u << 31)) // BL
			globals->cpu.reg[30] = globals->cpu.pc + 4;

		globals->cpu.pc += (int32_t)(instruction << 6) >> 4;
		return true;
	}
	else if((instruction & 0xFE000000) == 0xD6000000) // Unconditional branch (register)
	{
		if(instruction & 0x0000FC1F)
			goto unimpl; // undef?

		if((instruction & 0x001F0000) != 0x001F0000)
			goto unimpl; // undef?

		uint8_t opc = (instruction >> 21) & 0xF;
		switch(opc)
		{
		case 0b0001: // BLR
		{
			// Temporary variable to support BLR X30
			uint64_t dest = globals->cpu.reg[decode_rn(instruction)];
			globals->cpu.reg[30] = globals->cpu.pc + 4;
			globals->cpu.pc = dest;
			return true;
		}
		case 0b0010: // RET
		case 0b0000: // BR
			globals->cpu.pc = globals->cpu.reg[decode_rn(instruction)];
			return true;
		case 0b0100: // ERET
			handle_exception_return(globals);
			return true;
		default:
			goto unimpl;
		}
	}
	else if((instruction & 0xFE000000) == 0x54000000) // Conditional branch (immediate)
	{
		if(instruction & (1 << 4) || instruction & (1 << 24))
			goto unimpl;

		if(!test_condition(globals, (enum Condition)(instruction & 0xF)))
			goto instruction_done;

		globals->cpu.pc += (int32_t)(instruction << 8) >> 11;
		globals->cpu.pc &= ~3;
		return true;
	}
	else if((instruction & 0x1F000000) == 0x0A000000) // Logical (shifted register)
	{
		bool sf = instruction & (1u << 31),
		     n = instruction & (1 << 21);

		uint64_t op1 = get_reg_wzr(globals, decode_rn(instruction), sf),
		         op2 = shifted_register(globals, instruction, sf);

		uint64_t result = 0;

		if(n)
			op2 = ~op2;

		if(!sf)
			op2 = (uint32_t) op2;

		switch((instruction >> 29) & 3)
		{
		case 0b00:
			result = op1 & op2;
			break;
		case 0b01:
			result = op1 | op2;
			break;
		case 0b10:
			result = op1 ^ op2;
			break;
		case 0b11:
			result = op1 & op2;
			adjust_nzcv_value(globals, result, sf);
			break;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1F800000) == 0x12000000) // Logical (immediate)
	{
		bool sf = instruction & (1u << 31);
		uint8_t op = (instruction >> 29) & 3;

		uint64_t op1 = get_reg_wzr(globals, decode_rn(instruction), sf),
		         op2;

		if(!logical_immediate(instruction, &op2))
			goto undef;

		if(!sf)
			op2 = (uint32_t) op2;

		uint64_t result;

		switch(op)
		{
		case 0b00:
			result = op1 & op2;
			break;
		case 0b01:
			result = op1 | op2;
			break;
		case 0b10:
			result = op1 ^ op2;
			break;
		case 0b11:
			result = op1 & op2;
			adjust_nzcv_value(globals, result, sf);
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			goto instruction_done;
		default:
			__builtin_unreachable();
		}

		set_reg_sp(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1F000000) == 0x1B000000) // Data-processing (3 source)
	{
		bool sf = instruction & (1u << 31);
		uint8_t opcode = ((instruction >> 25) & 0b110000)
		        | ((instruction >> 20) & 0b1110)
		        | ((instruction >> 15) & 1);

		uint64_t rn = get_reg_wzr(globals, decode_rn(instruction), sf),
		         rm = get_reg_wzr(globals, decode_rm(instruction), sf),
		         ra = get_reg_wzr(globals, decode_ra(instruction), sf);

		uint64_t result;
		switch(opcode)
		{
		case 0b000000: // MADD
			if(sf)
				result = ra + rn * rm;
			else
				result = (uint32_t)ra + (uint32_t)rn * (uint32_t)rm;
			break;
		case 0b000001: // MSUB
			if(sf)
				result = ra - rn * rm;
			else
				result = (uint32_t)ra - (uint32_t)rn * (uint32_t)rm;
			break;
		case 0b001010: // UMADDL
			if(!sf) goto undef;
			result = ra + (uint64_t)(uint32_t)rn * (uint32_t)rm;
			break;
		case 0b001011: // UMSUBL
			if(!sf) goto undef;
			result = ra - (uint64_t)(uint32_t)rn * (uint32_t)rm;
			break;
		case 0b000010: // SMADDL
			if(!sf) goto undef;
			result = ra + (int64_t)(int32_t)rn * (int32_t)rm;
			break;
		case 0b000011: // SMSUBL
			if(!sf) goto undef;
			result = ra - (int64_t)(int32_t)rn * (int32_t)rm;
			break;
		case 0b001100: // UMULH
		{
			if(!sf) goto undef;
            #ifdef __OPENCL_VERSION__
			    result = mul_hi(rn, rm);
            #else
			    uint32_t w = rn >> 32, x = rn, y = rm >> 32, z = rm;
				uint64_t ea = (uint64_t)x * (uint64_t)z,
				         gf = (uint64_t)w * (uint64_t)z,
				         jh = (uint64_t)x * (uint64_t)y,
				         lk = (uint64_t)w * (uint64_t)y;

				result = (((ea >> 32) + gf + jh) >> 32) + lk;
            #endif
			break;
		}
		case 0b000100: // SMULH
		{
			if(!sf) goto undef;
			bool negative = false;

			if((int64_t)rn < 0)
			{
				negative = !negative;
				rn = -rn;
			}
			if((int64_t)rm < 0)
			{
				negative = !negative;
				rm = -rm;
			}

            #ifdef __OPENCL_VERSION__
			    result = mul_hi((int64_t)rn, (int64_t)rm);
            #else
			    uint32_t w = rn >> 32, x = rn, y = rm >> 32, z = rm;
				uint64_t ea = (uint64_t)x * (uint64_t)z,
				         gf = (uint64_t)w * (uint64_t)z,
				         jh = (uint64_t)x * (uint64_t)y,
				         lk = (uint64_t)w * (uint64_t)y;

				result = (((ea >> 32) + gf + jh) >> 32) + lk;
            #endif

			result = negative ? -result : result;
			break;
		}
		default:
			goto unimpl;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, true);
	}
	else if((instruction & 0x5FE00000) == 0x1AC00000) // Data-processing (2 source)
	{
		bool sf = instruction & (1u << 31);
		uint8_t opcode = (instruction >> 10) & 0x3F;

		if(instruction & (1 << 29))
			goto undef;

		uint64_t op1 = get_reg_wzr(globals, decode_rn(instruction), sf),
		         op2 = get_reg_wzr(globals, decode_rm(instruction), sf);

		uint64_t result;

		if((opcode >> 3) == 1) // Shift
			op2 = sf ? (op2 & 63) : (op2 & 31);

		switch(opcode)
		{
		case 0b001000: // LSLV
			result = op1 << op2;
			break;
		case 0b001001: // LSRV
			result = op1 >> op2;
			break;
		case 0b001010: // ASRV
			if(sf)
				result = (int64_t)op1 >> op2;
			else
				result = (int32_t)op1 >> op2;
			break;
		case 0b001011: // RORV
			if(!op2)
			{
				result = op1;
				break;
			}

			if(sf)
				result = (op1 >> op2) | (op1 << (64 - op2));
			else
				result = (op1 >> op2) | (op1 << (32 - op2));
			break;
		case 0b000010: // UDIV
			if(!op2)
				result = 0;
			else
				result = op1 / op2;
			break;
		case 0b000011: // SDIV
			if(!op2)
				result = 0;
			else if(sf)
				result = (int64_t)op1 / (int64_t)op2;
			else
				result = (int32_t)op1 / (int32_t)op2;
			break;
		default:
			goto unimpl;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x5FE00000) == 0x5AC00000) // Data-processing (1 source)
	{
		bool sf = instruction & (1u << 31);
		uint16_t opcode = (instruction >> 10) & 0x7FF;

		if(instruction & (1 << 29)) // "S"
			goto undef;

		uint64_t src = get_reg_wzr(globals, decode_rn(instruction), sf);
		uint64_t result = 0;

		switch(opcode)
		{
		case 0b00000000100: // CLZ
			result = sf ? clz64(src) : clz32(src);
			break;
		case 0b00000000101: // CLS
			// TODO: Not CLSetbits, it's CLSignbits
			result = sf ? clz64(~src) : clz32(~src);
			break;
		case 0b00000000010: // REV (32-bit)
			if(sf)
				goto undef;
			result = rev32(src);
			break;
		case 0b00000000011: // REV (64-bit)
			if(!sf)
				goto undef;
			result = rev64(src);
			break;
		case 0b00000000001: // REV16
			if(sf)
			{
				result |= (uint64_t)rev16(src >> 48) << 48
				          | (uint64_t)rev16(src >> 32) << 32;
			}

			result |= (uint32_t)rev16(src >> 16) << 16
			          | (uint32_t)rev16(src >> 0) << 0;

			break;
		case 0b00000000000: // RBIT
			if(sf)
			{
				result |= (uint64_t)revbits(src >> 56) << 56
				          | (uint64_t)revbits(src >> 48) << 48
				          | (uint64_t)revbits(src >> 40) << 40
				          | (uint64_t)revbits(src >> 32) << 32;
			}

			result |= (uint32_t)revbits(src >> 24) << 24
			          | (uint32_t)revbits(src >> 16) << 16
			          | (uint32_t)revbits(src >> 8) << 8
			          | (uint32_t)revbits(src >> 0) << 0;

			result = sf ? rev64(result) : rev32(result);
			break;
		default:
			goto unimpl;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1F200000) == 0x0B000000) // Add/subtract (shifted register)
	{
		bool sf = instruction & (1u << 31);
		uint64_t op1 = get_reg_wzr(globals, decode_rn(instruction), sf),
		         op2 = shifted_register(globals, instruction, sf);

		uint64_t result = 0;

		switch((instruction >> 29) & 3)
		{
		case 0b00: // ADD
			result = op1 + op2;
			break;
		case 0b01: // ADDS
			result = add_setcc(globals, op1, op2, false, sf);
			break;
		case 0b10: // SUB
			result = op1 - op2;
			break;
		case 0b11: // SUBS
			result = add_setcc(globals, op1, ~op2, true, sf);
			break;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1F200000) == 0x0B200000) // Add/subtract (extended register)
	{
		bool sf = instruction & (1u << 31);

		uint8_t imm3 = (instruction >> 10) & 0b111,
		        option = (instruction >> 13) & 0b111,
		        opt = (instruction >> 22) & 0b11;

		if(opt != 0b00 || imm3 > 4)
			goto unimpl;

		uint64_t op1 = get_reg_sp(globals, decode_rn(instruction), sf),
		         rm = get_reg_wzr(globals, decode_rm(instruction), (option & 0b11) == 0b11),
		         op2 = extended_register(rm, option, imm3);

		uint64_t result = 0;

		switch((instruction >> 29) & 3)
		{
		case 0b00: // ADD
			result = op1 + op2;
			set_reg_sp(globals, decode_rd(instruction), result, sf);
			break;
		case 0b01: // ADDS
			result = add_setcc(globals, op1, op2, false, sf);
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			break;
		case 0b10: // SUB
			result = op1 - op2;
			set_reg_sp(globals, decode_rd(instruction), result, sf);
			break;
		case 0b11: // SUBS
			result = add_setcc(globals, op1, ~op2, true, sf);
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			break;
		}
	}
	else if((instruction & 0x1FE00000) == 0x1A000000) // Add/subtract (with carry)
	{
		bool sf = instruction & (1u << 31);

		uint8_t opcode = (instruction >> 10 & 0x3F);
		if(opcode != 0)
			goto unimpl;

		uint64_t op1 = get_reg_wzr(globals, decode_rn(instruction), sf),
		         op2 = get_reg_wzr(globals, decode_rm(instruction), sf);

		uint64_t result = 0;

		switch((instruction >> 29) & 3)
		{
		case 0b00: // ADC
			result = op1 + op2 + globals->cpu.nzcv.c;
			break;
		case 0b01: // ADCS
			result = add_setcc(globals, op1, op2, globals->cpu.nzcv.c, sf);
			break;
		case 0b10: // SBC
			result = op1 - op2 - 1 + globals->cpu.nzcv.c;
			break;
		case 0b11: // SBCS
			result = add_setcc(globals, op1, ~op2, globals->cpu.nzcv.c, sf);
			break;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1F800000) == 0x12800000) // Move wide (immediate)
	{
		bool sf = instruction & (1u << 31);
		uint8_t rd = decode_rd(instruction);
		uint8_t hw = (instruction >> 21) & 0b11;
		uint64_t imm = (instruction >> 5) & 0xFFFF;

		if(!sf && hw > 1)
			goto unimpl;

		imm <<= hw * 16;

		switch((instruction >> 29) & 0b11)
		{
		case 0b00: // MOVN
			imm = ~imm;
			break;
		case 0b10: // MOVZ
			break;
		case 0b11: // MOVK
		{
			uint64_t old = get_reg_wzr(globals, rd, sf);
			old &= ~(0xFFFFul << (hw*16));
			imm |= old;
			break;
		}
		case 0b01:
			goto unimpl;
		}

		set_reg_wzr(globals, rd, imm, sf);
	}
	else if((instruction & 0x1FE00000) == 0x1A800000) // Conditional select
	{
		bool sf = instruction & (1u << 31),
		     op = instruction & (1 << 30),
		     s = instruction & (1 << 29);

		uint8_t op2 = (instruction >> 10) & 3;

		if(s || op2 & 0b10)
			goto undef;

		enum Condition cond = (instruction >> 12) & 0xF;

		uint64_t result;

		if(test_condition(globals, cond))
			result = get_reg_wzr(globals, decode_rn(instruction), sf);
		else
		{
			result = get_reg_wzr(globals, decode_rm(instruction), sf);
			if(op)
				result = ~result;
			if(op2 & 0b01)
				result += 1;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x1FE00000) == 0x1A400000) // Conditional compare
	{
		bool sf = instruction & (1u << 31),
		     op = instruction & (1 << 30),
		     s = instruction & (1 << 29),
		     imm = instruction & (1 << 11),
		     o2 = instruction & (1 << 10),
		     o3 = instruction & (1 << 4);

		enum Condition condition = (instruction >> 12) & 0xF;

		if(!s || o2 || o3)
			goto undef;

		if(!test_condition(globals, condition))
		{
			globals->cpu.nzcv.n = instruction & 0b1000;
			globals->cpu.nzcv.z = instruction & 0b0100;
			globals->cpu.nzcv.c = instruction & 0b0010;
			globals->cpu.nzcv.v = instruction & 0b0001;
			goto instruction_done;
		}

		uint64_t left = get_reg_wzr(globals, decode_rn(instruction), sf),
		         right;

		if(imm)
			right = (instruction >> 16) & 0x3F;
		else
			right = get_reg_wzr(globals, decode_rm(instruction), sf);

		if(op) // CCMP
			(void) add_setcc(globals, left, ~right, true, sf);
		else // CCMN
			(void) add_setcc(globals, left, right, false, sf);
	}
	else if((instruction & 0x7E000000) == 0x34000000) // Compare and branch
	{
		bool sf = instruction & (1u << 31),
		     op = instruction & (1 << 24);

		uint64_t value = get_reg_wzr(globals, decode_rt(instruction), sf);

		if((value != 0) != op)
			goto instruction_done;

		globals->cpu.pc += (int32_t)(instruction << 8) >> 11;
		globals->cpu.pc &= ~3;
		return true;
	}
	else if((instruction & 0x7E000000) == 0x36000000) // Test and branch
	{
		bool op = instruction & (1 << 24);

		uint8_t bit = ((instruction >> 26) & 0x20) | ((instruction >> 19) & 0x1F);

		uint64_t value = get_reg_wzr(globals, decode_rt(instruction), true);

		if(!!(value & (1ul << bit)) != op)
			goto instruction_done;

		globals->cpu.pc += (int32_t)(instruction << 13) >> 16;
		globals->cpu.pc &= ~3;
		return true;
	}
	else if((instruction & 0x1F000000) == 0x10000000) // PC-rel. addressing
	{
		bool page = instruction & (1u << 31);
		int64_t imm = ((uint64_t)(instruction & 0x60000000) << 14) | ((uint64_t)(instruction & 0x00FFFFE0) << 40);

		imm >>= 31;

		uint64_t base = globals->cpu.pc;
		if(page)
			base = (base & ~0xFFF) + imm;
		else
			base += imm >> 12;

		set_reg_sp(globals, decode_rd(instruction), base, true);
	}
	else if((instruction & 0x1F000000) == 0x11000000) // Add/subtract (immediate)
	{
		bool sf = instruction & (1u << 31);
		uint64_t op1 = ((instruction >> 29) & 1)
		                  ? get_reg_wzr(globals, decode_rn(instruction), true)
		                  : get_reg_sp(globals, decode_rn(instruction), true),
		         op2 = shifted_immediate(instruction);

		uint64_t result;

		switch((instruction >> 29) & 3)
		{
		case 0b00: // ADD
			result = op1 + op2;
			set_reg_sp(globals, decode_rd(instruction), result, sf);
			break;
		case 0b01: // ADDS
			result = add_setcc(globals, op1, op2, false, sf);
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			break;
		case 0b10: // SUB
			result = op1 - op2;
			set_reg_sp(globals, decode_rd(instruction), result, sf);
			break;
		case 0b11: // SUBS
			result = add_setcc(globals, op1, ~op2, true, sf);
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			break;
		}
	}
	else if((instruction & 0x1F800000) == 0x13000000) // Bitfield move
	{
		bool sf = instruction & (1u << 31),
		     n = instruction & (1 << 22);

		uint8_t immr = (instruction >> 16) & 0x3F,
		        imms = (instruction >> 10) & 0x3F,
		        opc = (instruction >> 29) & 0b11;

		if(sf != n
		   || (!sf && immr >= 32) || (!sf && imms >= 32)
		   || opc == 0b11)
			goto undef;

		uint64_t result = opc == 0b01 ? get_reg_wzr(globals, decode_rd(instruction), sf) : 0,
		         input = get_reg_wzr(globals, decode_rn(instruction), sf);

		if(immr > imms) // BFI
		{
			uint8_t datasize = 32 << sf;

			uint64_t mask = ~(~0ul << (imms+1));
			if(immr)
			{
				input = (input >> immr) | (input << (datasize - immr));
				mask = (mask >> immr) | (mask << (datasize - immr));
			}

			result = (result & ~mask) | (input & mask);
			if(opc == 0b00) // SBFIZ
			{
				if(result & (1ul << (datasize - immr + imms)))
					result |= ~0ul << (datasize - immr + imms + 1);
			}
			set_reg_wzr(globals, decode_rd(instruction), result, sf);
			goto instruction_done;
		}

		uint64_t tmask = 0xFFFFFFFFFFFFFFFFul << (imms - immr + 1);

		input >>= immr;

		if(opc == 0b00) // SBFM
		{
			result = input & ~tmask;
			if(input & (1ul << (imms - immr)))
				result |= tmask;
		}
		else // BFM
		{
			uint64_t tmask = 0xFFFFFFFFFFFFFFFFul << (imms - immr + 1);
			result &= tmask;
			result |= ~tmask & input;
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0x3E000000) == 0x38000000) // Load/store register (non-SIMD)
	{
		bool writeback = false,
		     postindex = false,
		     vector = instruction & (1 << 26);

		uint8_t opc = (instruction >> 22) & 3,
		        size = instruction >> 30;
		int64_t offset = 0;

		if(vector)
			size |= (opc & 0b10) << 1;

		if(instruction & (1 << 24)) // Unsigned immediate
		{
			writeback = postindex = false;
			offset = (instruction >> 10) & 0xFFF;
			offset <<= size;
		}
		else if(instruction & (1 << 21)) // Register offset
		{
			uint8_t option = (instruction >> 13) & 0x7;
			bool s = instruction & (1 << 12);
			uint64_t rm = get_reg_wzr(globals, decode_rm(instruction), (option & 0b11) == 0b11);

			offset = extended_register(rm, option, s ? size : 0);
		}
		else
		{
			writeback = instruction & (1 << 10);
			postindex = ((instruction >> 10) & 0b11) == 0b01;

			if(((instruction >> 10) & 0b11) == 0b10) // Unprivileged
				goto unimpl;

			int16_t imm = (instruction & 0x001FF000) >> 5; imm >>= 7;
			offset = imm;
		}

		uint64_t base = get_reg_sp(globals, decode_rn(instruction), true);

		if(!postindex)
			base += offset;

		bool load = opc & 1;
		bool success = true;
		switch((size << 2) | opc)
		{
		case 0b0000: // STRB
		{
			uint8_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_byte(globals, base, value, &success);
			break;
		}
		case 0b0001: // LDRB
		{
			uint8_t value = read_byte(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, true);
			break;
		}
		case 0b0010: // LDRSB (64-bit)
		{
			load = true;
			int8_t value = read_byte(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), (int64_t)value, true);
			break;
		}
		case 0b0011: // LDRSB (32-bit)
		{
			int8_t value = read_byte(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), (int32_t)value, false);
			break;
		}
		case 0b0100: // STRH
		{
			uint16_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_half(globals, base, value, &success);
			break;
		}
		case 0b0101: // LDRH
		{
			uint16_t value = read_half(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, true);
			break;
		}
		case 0b0110: // LDRSH (64-bit)
		{
			load = true;
			int16_t value = read_half(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), (int64_t)value, true);
			break;
		}
		case 0b0111: // LDRSH (32-bit)
		{
			int16_t value = read_half(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), (int32_t)value, false);
			break;
		}
		case 0b1000: // STRW
		{
			uint32_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_word(globals, base, value, &success);
			break;
		}
		case 0b1001: // LDRW
		{
			uint32_t value = read_word(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, true);
			break;
		}
		case 0b1010: // LDRSW
		{
			load = true;
			int32_t value = read_word(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), (int64_t)value, true);
			break;
		}
		case 0b1100: // STR
		{
			uint64_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_dword(globals, base, value, &success);
			break;
		}
		case 0b1101: // LDR
		{
			uint64_t value = read_dword(globals, base, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, true);
			break;
		}
		case 0b1110: // PRFM
			break;
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, !load);
			return true;
		}
		else if(writeback)
		{
			if(postindex)
				base += offset;

			set_reg_sp(globals, decode_rn(instruction), base, true);
		}
	}
	else if((instruction & 0x3A000000) == 0x28000000) // Load/store register pair
	{
		bool load = instruction & (1 << 22),
		     writeback = instruction & (1 << 23),
		     postindex = !(instruction & (1 << 24)),
		     vector = instruction & (1 << 26);
		uint8_t opc = instruction >> 30;

		if(vector)
			goto fp_instruction;

		uint8_t scale = 2 + (opc >> 1);

		if(!writeback && postindex) // No-allocate
			postindex = false;

		int32_t offset = (instruction & 0x003F8000) << 10; offset >>= 25;
		offset *= 1<<scale;

		uint64_t base = get_reg_sp(globals, decode_rn(instruction), true),
		        address = base;
		if(!postindex)
			address += offset;

		bool success = true;
		uint64_t value, value2;
		switch(opc << 1 | load)
		{
		case 0b001: // LDP (32-bit)
			value = read_word(globals, address, &success);
			if(success)
			{
				address += 4;
				value2 = read_word(globals, address, &success);
			}

			if(success)
			{
				set_reg_wzr(globals, decode_rt(instruction), value, false);
				set_reg_wzr(globals, decode_rt2(instruction), value2, false);
			}
			break;
		case 0b101: // LDP (64-bit)
			value = read_dword(globals, address, &success);
			if(success)
			{
				address += 8;
				value2 = read_dword(globals, address, &success);
			}

			if(success)
			{
				set_reg_wzr(globals, decode_rt(instruction), value, true);
				set_reg_wzr(globals, decode_rt2(instruction), value2, true);
			}
			break;
		case 0b000: // STP 32-bit
			write_word(globals, address, get_reg_wzr(globals, decode_rt(instruction), false), &success);
			if(success)
			{
				address += 4;
				write_word(globals, address, get_reg_wzr(globals, decode_rt2(instruction), false), &success);
			}
			break;
		case 0b100: // STP 64-bit
			write_dword(globals, address, get_reg_wzr(globals, decode_rt(instruction), true), &success);
			if(success)
			{
				address += 8;
				write_dword(globals, address, get_reg_wzr(globals, decode_rt2(instruction), true), &success);
			}
			break;
		case 0b011: // LDPSW
			value = (int64_t)(int32_t)read_word(globals, address, &success);
			if(success)
			{
				address += 4;
				value2 = (int64_t)(int32_t)read_word(globals, address, &success);
			}

			if(success)
			{
				set_reg_wzr(globals, decode_rt(instruction), value, true);
				set_reg_wzr(globals, decode_rt2(instruction), value2, true);
			}
			break;
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, !load);
			return true;
		}

		if(writeback)
			set_reg_sp(globals, decode_rn(instruction), base + offset, true);
	}
	else if((instruction & 0x3F000000) == 0x08000000) // Load/store exclusive
	{
		bool o0 = instruction & (1 << 15),
		     o1 = instruction & (1 << 21),
		     l = instruction & (1 << 22),
		     o2 = instruction & (1 << 23);

		uint8_t size = instruction >> 30;

		uint64_t address = get_reg_sp(globals, decode_rn(instruction), true);
		bool success = true;

		switch(size << 4 | (o2 << 3) | (l << 2) | (o1 << 1) | o0)
		{
		case 0b111001: // STLR (64-bit)
		{
			uint64_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_dword(globals, address, value, &success);
			break;
		}
		case 0b110001: // STLXR (64-bit)
		case 0b110000: // STXR (64-bit)
		{
			uint64_t value = get_reg_wzr(globals, decode_rt(instruction), true);
			write_dword(globals, address, value, &success);
			set_reg_wzr(globals, decode_rs(instruction), !success, true);
			break;
		}
		case 0b101001: // STLR (64-bit)
		{
			uint32_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_word(globals, address, value, &success);
			break;
		}
		case 0b100001: // STLXR (32-bit)
		case 0b100000: // STXR (32-bit)
		{
			uint32_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_word(globals, address, value, &success);
			set_reg_wzr(globals, decode_rs(instruction), !success, true);
			break;
		}
		case 0b111101: // LDAR (64-bit)
		case 0b110101: // LDAXR (64-bit)
		case 0b110100: // LDXR (64-bit)
		{
			uint64_t value = read_dword(globals, address, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, true);
			break;
		}
		case 0b101101: // LDAR (32-bit)
		case 0b100101: // LDAXR (32-bit)
		case 0b100100: // LDXR (32-bit)
		{
			uint32_t value = read_word(globals, address, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, false);
			break;
		}
		//case 0b011101: // LDARH
		//case 0b010101: // LDAXRH
		case 0b010100: // LDXRH
		{
			uint16_t value = read_half(globals, address, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, false);
			break;
		}
		case 0b010000: // STXRH
		{
			uint16_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_half(globals, address, value, &success);
			set_reg_wzr(globals, decode_rs(instruction), !success, true);
			break;
		}
		case 0b011001: // STLRH
		{
			uint16_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_half(globals, address, value, &success);
			break;
		}
		case 0b001101: // LDARB
		case 0b000100: // LDXRB
		{
			uint8_t value = read_byte(globals, address, &success);
			if(success)
				set_reg_wzr(globals, decode_rt(instruction), value, false);
			break;
		}
		case 0b001001: // STLRB
		{
			uint8_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_byte(globals, address, value, &success);
			break;
		}
		case 0b000000: // STXRB
		{
			uint8_t value = get_reg_wzr(globals, decode_rt(instruction), false);
			write_byte(globals, address, value, &success);
			set_reg_wzr(globals, decode_rs(instruction), !success, true);
			break;
		}
		case 0b110110: // LDXP
		{
			uint64_t value1 = read_dword(globals, address, &success), value2;

			if(success)
				value2 = read_dword(globals, address + 8, &success);

			if(success)
			{
				set_reg_wzr(globals, decode_rt(instruction), value1, true);
				set_reg_wzr(globals, decode_rt2(instruction), value2, true);
			}
			break;
		}
		case 0b110011: // STLXP
		case 0b110010: // STXP
		{
			uint64_t value1 = get_reg_wzr(globals, decode_rt(instruction), true),
			         value2 = get_reg_wzr(globals, decode_rt2(instruction), true);

			write_dword(globals, address, value1, &success);
			if(success)
				write_dword(globals, address + 8, value2, &success);

			set_reg_wzr(globals, decode_rs(instruction), !success, true);
			break;
		}
		default:
			goto unimpl;
		}

		if(!success)
		{
			handle_data_abort(globals, !l);
			return true;
		}
	}
	else if((instruction & 0x3F000000) == 0x18000000) // LDR (literal)
	{
		uint8_t opc = instruction >> 30;
		int32_t offset = (int32_t)(instruction << 8) >> 13;
		offset <<= 2;

		bool success = true;
		uint64_t address = globals->cpu.pc + offset;
		uint64_t value;

		switch(opc)
		{
		case 0b00:
			value = read_word(globals, address, &success);
			break;
		case 0b01:
			value = read_dword(globals, address, &success);
			break;
		case 0b10:
			value = (int64_t)(int32_t)read_word(globals, address, &success);
			break;
		case 0b11: // Prefetch
			goto instruction_done;
		}

		if(!success)
		{
			printf("Data abort\n");
			goto unimpl;
		}

		set_reg_wzr(globals, decode_rd(instruction), value, true);
	}
	else if((instruction & 0x7FA00000) == 0x13800000) // EXTR
	{
		bool sf = instruction & (1u << 31),
		     n = instruction & (1 << 22);

		uint8_t imms = (instruction >> 10) & 0x3F;

		if(sf != n)
			goto undef;

		if(!sf && imms >= 32)
			goto undef;

		uint64_t rn = get_reg_wzr(globals, decode_rn(instruction), sf),
		         rm = get_reg_wzr(globals, decode_rm(instruction), sf);

		uint64_t result = rm;

		if(imms > 0)
		{
			uint8_t bits = 32 << sf;
			result = (rn << (bits - imms)) | (rm >> imms);
		}

		set_reg_wzr(globals, decode_rd(instruction), result, sf);
	}
	else if((instruction & 0xFFC00000) == 0xD5000000) // System instructions
	{
		bool l = instruction & (1 << 21);
		uint8_t op0 = (instruction >> 19) & 0b11,
		        op1 = (instruction >> 16) & 0b111,
		        op2 = (instruction >> 5) & 0b111,
		        crn = (instruction >> 12) & 0xF,
		        crm = (instruction >> 8) & 0xF;
		uint16_t sysreg = (instruction >> 5) & 0xFFFF;

		uint64_t value;

		// MSR (immediate)
		if(!l && op0 == 0 && crn == 0b0100 && decode_rt(instruction) == 31)
		{
			uint64_t imm = crm;

			switch((op1 << 3) | op2)
			{
			case 0b000101: // SP
				value = imm;
				sysreg = 0b0000001000001101;
				break;
			case 0b011110: // DAIFSet
				value = globals->cpu.DAIF | (imm << 6);
				sysreg = 0b1101101000010001;
				break;
			case 0b011111: // DAIFClr
				value = globals->cpu.DAIF & ~(imm << 6);
				sysreg = 0b1101101000010001;
				break;
			default:
				goto unimpl;
			}
		}
		else if(!l)
			value = get_reg_wzr(globals, decode_rd(instruction), true);

		if((sysreg & 0xF800) == 0x1800) // HINT, CLREX, DSB, DMB, ISB -> nop
			goto instruction_done;

		if(l)
		{
			switch(sysreg)
			{
			case 0b1101100000000001: // CTR_EL0
				value = 0x111C001;
				break;
			case 0b1100100000000000: // CCSIDR_EL1
				value = 0x2; // No features, 32bit cache line size
				break;
			case 0b0000001000001101: // SPSel
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.SPSel;
				break;
			case 0b1100001000010010: // CurrentEL
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.current_el << 2;
				break;
			case 0b1100000010000000: // SCTLR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.SCTLR_EL1;
				break;
			case 0b1100000000101000: // ID_AA64DFR0_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0b00010000000100000001000000000110;
				break;
			case 0b1100000000111000: // ID_AA64MMFR0_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0x0F000005;
				break;
			case 0b1100000000100000: // ID_AA64PFR0_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0x1000011; // Only AArch64 EL1 and EL0. FP. AdvSIMD. GICv3.
				break;
			case 0b1100000000110000: // ID_AA64_ISAR0_EL1
			case 0b1100000000110001: // ID_AA64_ISAR1_EL1
			case 0b1100000000110010: // ID_AA64_ISAR2_EL1
			case 0b1100000000111001: // ID_AA64MMFR1_EL1
			case 0b1100000000111010: // ID_AA64MMFR2_EL1
			case 0b1100000000111011: // ID_AA64MMFR3_EL1
			case 0b1100000000100001: // ID_AA64PFR1_EL1
			case 0b1100000000100101: // ID_AA64SMFR0_EL1
			case 0b1100000000010000: // ID_ISAR0_EL1
			case 0b1100000000010001: // ID_ISAR1_EL1
			case 0b1100000000010010: // ID_ISAR2_EL1
			case 0b1100000000010011: // ID_ISAR3_EL1
			case 0b1100000000010100: // ID_ISAR4_EL1
			case 0b1100000000010101: // ID_ISAR5_EL1
			case 0b1100000000001100: // ID_MMFR0_EL1
			case 0b1100000000001101: // ID_MMFR1_EL1
			case 0b1100000000001110: // ID_MMFR2_EL1
			case 0b1100000000001111: // ID_MMFR3_EL1
			case 0b1100000000001000: // ID_PFR0_EL1
			case 0b1100000000001001: // ID_PFR1_EL1
			case 0b1100000000001010: // ID_DFR0_EL1
			case 0b1100000000101001: // ID_AA64_DFR1_EL1
			case 0b1100000000011000: // MVFR0_EL1
			case 0b1100000000011001: // MVFR1_EL1
			case 0b1100000000011010: // MVFR2_EL1
			case 0b1100000000100100: // ID_AA64ZFR0_EL1 (undocumented?)
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0;
				break;
			case 0b1100010100010000: // MAIR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.MAIR_EL1;
				break;
			case 0b1100000100000010: // TCR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.TCR_EL1;
				break;
			case 0b1100000100000000: // TTBR0_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.TTBR0_EL1;
				break;
			case 0b1100000100000001: // TTBR1_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.TTBR1_EL1;
				break;
			case 0b1100011000000000: // VBAR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.VBAR_EL1;
				break;
			case 0b1100001000001000: // SP_EL0
				if(!globals->cpu.SPSel || globals->cpu.current_el == 0)
					value = globals->cpu.reg[31];
				else
					value = globals->cpu.sp_el[0];
				break;
			case 0b1100000000000101: // MPIDR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->pe_id;
				break;
			case 0b1100000000000000: // MIDR_EL1
				if(0&&globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0xFA0F0000;
				break;
			case 0b1100000000000110: // REVIDR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 0x71FEBEEF;
				break;
			case 0b1101111010000011: // TPIDRRO_EL0:
				value = globals->cpu.TPIDRRO_EL0;
				break;
			case 0b1101111010000010: // TPIDR_EL0:
				value = globals->cpu.TPIDR_EL0;
				break;
			case 0b1100011010000100: // TPIDR_EL1:
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.TPIDR_EL1;
				break;
			case 0b1101100000000111: // DCZID_EL0
				// DC ZVA clears 64 bytes
				value = 4;
				break;
			case 0b1101101000010001: // DAIF
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.DAIF;
				break;
			case 0b1100011100001000: // CNTKCTL_EL1;
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.CNTKCTL_EL1;
				break;
			case 0b1101111100000000: // CNTFRQ_EL0
				value = globals->cpu.CNTFRQ_EL0;
				break;
			case 0b1101111100011001: // CNTV_CTL_EL0
				value = globals->cpu.CNTV_CTL_EL0;
				break;
			case 0b1101111100000010: // CNTVCT_EL0
				value = globals->cpu.timer_counter - globals->cpu.timer_offset;
				break;
			case 0b1100011001100100: // ICC_CTLR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.ICC_CTRLR_EL1;
				break;
			case 0b1100011001100101: // ICC_SRE_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 7; // Disable IRQ bypass, FIQ bypass and enable system register access
				break;
			case 0b1100001000110000: // ICC_PMR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.ICC_PMR_EL1;
				break;
			case 0b1100011001100000: // ICC_IAR1_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.ICC_IAR1_EL1;
				gicv3_ack_interrupt(globals, value);
				break;
			case 0b1100001000000001: // ELR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.ELR_EL[1-1];
				break;
			case 0b1100001000000000: // SPSR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.SPSR_EL[1-1];
				break;
			case 0b1100001010010000: // ESR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.ESR_EL[1-1];
				break;
			case 0b1100001100000000: // FAR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = globals->cpu.FAR_EL[1-1];
				break;
			case 0b1100100000000001: // CLIDR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				value = 4;
				break;
			case 0b1101101000100000: // FPCR
				value = globals->cpu.FPCR;
				break;
			case 0b1101101000100001: // FPSR
				value = globals->cpu.FPSR;
				break;
			default:
				printf("Read system register: %x (%d %d %d %d %d)\n", sysreg, op0, op1, crn, crm, op2);
				goto unimpl;
			}

			set_reg_wzr(globals, decode_rd(instruction), value, true);
		}
		else
		{
			switch(sysreg)
			{
			case 0b0100001110110001: // DC IVAC
			case 0b0101101111110001: // DC CIVAC
			case 0b0101101111011001: // DC CVAU
			case 0b0101101111010001: // DC CVAC
				break;
			case 0b0100010000111000: // TLBI
			case 0b0100010000111011: // TLBI VAAE1
			case 0b0100010000011011: // TLBI VAAE1IS
			case 0b0100010000011111: // TLBI VAALE1IS
			case 0b0100010000011000: // TLBI VMALLE1IS
			case 0b0100010000011001: // TLBI VAE1IS
			case 0b0100010000011101: // TLBI VALE1IS
			case 0b0100010000011010: // TLBI ASIDE1IS
				flush_addr_cache(globals);
				break;
			case 0b0100001110001000: // IC IALLUIS
			case 0b0100001110101000: // IC IALLU
			case 0b0101101110101001: // IC IVAU
				break;
			case 0b0101101110100001: // DC ZVA
			{
				uint64_t address = value & ~(uint64_t)63;
				global uint64_t *ptr = virt_ptr(globals, address, true);
				if(!ptr)
				{
					// Not quite correct: If virt_ptr returns NULL because it points into MMIO,
					// ESR is not set.
					handle_data_abort(globals, true);
					return true;
				}

				for(int i = 8; i--;)
					*ptr++ = 0;

				break;
			}
			case 0b0000001000001101: // SPSel_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0] = globals->cpu.reg[31];
				globals->cpu.SPSel = value & 1;
				globals->cpu.reg[31] = globals->cpu.sp_el[globals->cpu.SPSel ? globals->cpu.current_el : 0];
				break;
			case 0b1100000010000000: // SCTLR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.SCTLR_EL1 = value;
				break;
			case 0b1100000010000010: // CPACR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.CPACR_EL1 = value;
				break;
			case 0b1000000000010010: // MDSCR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?
				break;
			case 0b1100010100010000: // MAIR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.MAIR_EL1 = value;
				break;
			case 0b1100000100000010: // TCR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.TCR_EL1 = value;
				flush_addr_cache(globals);
				break;
			case 0b1100000100000000: // TTBR0_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.TTBR0_EL1 = value;
				flush_addr_cache(globals);
				break;
			case 0b1100000100000001: // TTBR1_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.TTBR1_EL1 = value;
				flush_addr_cache(globals);
				break;
			case 0b1100011000000000: // VBAR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.VBAR_EL1 = value;
				break;
			case 0b1100001100000000: // FAR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.FAR_EL[1-1] = value;
				break;
			case 0b1101111010000011: // TPIDRRO_EL0:
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.TPIDRRO_EL0 = value;
				break;
			case 0b1101111010000010: // TPIDR_EL0:
				globals->cpu.TPIDR_EL0 = value;
				break;
			case 0b1100011010000100: // TPIDR_EL1:
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.TPIDR_EL1 = value;
				break;
			case 0b1100001000001000: // SP_EL0
				globals->cpu.sp_el[0] = value;
				if(!globals->cpu.SPSel || globals->cpu.current_el == 0)
					globals->cpu.reg[31] = value;
				break;
			case 0b1101101000010001: // DAIF
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.DAIF = value;
				break;
			case 0b1100011100001000: // CNTKCTL_EL1;
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.CNTKCTL_EL1 = value;
				break;
			case 0b1101111100011001: // CNTV_CTL_EL0
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.CNTV_CTL_EL0 = value;
				break;
			case 0b1101111100011000: // CNTV_TVAL_EL0
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				int64_t value64 = (int32_t)value;
				globals->cpu.timer_compare = (globals->cpu.timer_counter - globals->cpu.timer_offset) + value64;
				break;
			case 0b1101111100011010: // CNTV_CVAL_EL0
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.timer_compare = value;
				break;
			case 0b1100001000110000: // ICC_PMR_EL1
				globals->cpu.ICC_PMR_EL1 = value;
				break;
			case 0b1100011001100011: // ICC_BPR1_EL1
				globals->cpu.ICC_BPR1_EL1 = value;
				break;
			case 0b1100011001100100: // ICC_CTLR_EL1
				globals->cpu.ICC_CTRLR_EL1 = value;
				break;
			case 0b1100011001100111: // ICC_OGRPEN1_EL1
				globals->cpu.ICC_IGRPEN1_EL1 = value;
				break;
			case 0b1100011001100001: // ICC_EOIR1_EL1
				// Ignored
				break;
			case 0b1100001000000000: // ELR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.SPSR_EL[1-1] = value;
				break;
			case 0b1100001000000001: // SPSR_EL1
				if(globals->cpu.current_el == 0)
					goto unimpl; // Exception?

				globals->cpu.ELR_EL[1-1] = value;
				break;
			case 0b1000000010011100: // OSDLAR_EL1
			case 0b1000000010000100: // OSLAR_EL1
				break;
			case 0b1101101000100000: // FPCR
				globals->cpu.FPCR = value;
				break;
			case 0b1101101000100001: // FPSR
				globals->cpu.FPSR = value;
				break;
			case 0b1100011010000001: // CONTEXTIDR_EL1
				printf("CTXID: 0x%lx\n", value);
				break;
			case 0b1000000000000100: // DBGBVR0_EL1
			case 0b1000000000000101: // DBGBCR0_EL1
			case 0b1000000000000110: // DBGWVR0_EL1
			case 0b1000000000000111: // DBGWCR0_EL1
			case 0b1000000000001100: // DBGBVR1_EL1
			case 0b1000000000001101: // DBGBCR1_EL1
			case 0b1000000000001110: // DBGWVR1_EL1
			case 0b1000000000001111: // DBGWCR1_EL1
				// Debug registers - ignore
				break;
			case 0b1101000000000000: // CSSELR_EL1
				// Cache registers - ignore
				break;
			default:
				printf("Write system register: %x (%d %d %d %d %d)\n", sysreg, op0, op1, crn, crm, op2);
				goto unimpl;
			}
		}
	}
	else if((instruction & 0xFF000000) == 0xD4000000) // Exception generation
	{
		uint8_t opc = (instruction >> 21) & 0x7,
		        op2 = (instruction >> 2) & 0x7,
		        ll = instruction & 0x3;

		uint16_t imm = instruction >> 5;

		// The kernel still reads this. Bug or undocumented behaviour?
		globals->cpu.FAR_EL[1-1] = globals->cpu.pc;

		switch(opc << 5 | op2 << 2 | ll)
		{
		case 0b00000001: // SVC
			/*printf("Syscall %ld\n", globals->cpu.reg[8]);
			if(globals->cpu.reg[8] == 0x42) // writev
			{
				unsigned int count = globals->cpu.reg[2];
				uint64_t offset = 0;
				while(count--) {
				struct {uint64_t base; uint64_t len;} *ptr = virt_ptr(globals, globals->cpu.reg[1] + offset, false);
				offset += 16;
				const char *str = virt_ptr(globals, ptr->base, false);
				if(str)
					fwrite(str, 1, ptr->len, stdout);
				}
			}
			if(globals->cpu.reg[8] == 64) // write
			{
				const char *str = virt_ptr(globals, globals->cpu.reg[1], false);
				if(str)
					fwrite(str, 1, globals->cpu.reg[2], stdout);
			}
			if(globals->cpu.reg[8] == 63) // read
				printf("read(%ld, %lx, %ld)\n", globals->cpu.reg[0], globals->cpu.reg[1], globals->cpu.reg[2]);
			if(globals->cpu.reg[8] == 56) // openat
				printf("openat(%ld, %s, %lx)\n", globals->cpu.reg[0], (char*)virt_ptr(globals, globals->cpu.reg[1], false), globals->cpu.reg[2]);
			if(globals->cpu.reg[8] == 98) // futex
				printf("futex(%lx, %lx, %lx, %lx, %lx, %lx)\n",globals->cpu.reg[0], globals->cpu.reg[1], globals->cpu.reg[2], globals->cpu.reg[3], globals->cpu.reg[4], globals->cpu.reg[5]);
			if(globals->cpu.reg[8] == 29) // ioctl
				printf("ioctl(%ld, 0x%lx, 0x%lx)\n", globals->cpu.reg[0], globals->cpu.reg[1], globals->cpu.reg[2]);*/

			handle_exception(globals, globals->cpu.pc+4, 1, 0x15<<26 | 1<<25 | imm, 0);
			break;
		case 0b00100000: // BRK
			handle_exception(globals, globals->cpu.pc, 1, 0x3c<<26 | 1<<25 | imm, 0);
			break;
		case 0b01000000: // HLT
			if(imm == 0xf000 && globals->cpu.reg[0] == 3) // Semihosting: Print char
			{
				bool success = true;
				uint8_t c = read_byte(globals, globals->cpu.reg[1], &success);
                #ifdef __OPENCL_VERSION__
				    printf("%c", c);
                #else
				    putc(c, stdout);
                #endif

				break;
			}
			else
			{
				printf("Halted: %x\n", imm);
				goto unimpl;
			}
			break;
		default:
			goto unimpl;
		}

		return true;
	}
	else
	{
        fp_instruction:
#ifndef NO_FP_INTERPRETER
		if(interpret_fp_instruction(globals, instruction))
			return true;
#endif

        unimpl:
		printf("Unknown instruction %08x at %016lx (=0x%016lx)\n", instruction, globals->cpu.pc, translate_address(globals, globals->cpu.pc, false, 0));
		dump_cpu_state(&globals->cpu);
		return false;

        undef:
		printf("Undefined?\n");
		goto unimpl;
	}

    instruction_done:
	globals->cpu.pc += 4;
	return true;
}

kernel void interpret(global struct cpu *cpu_states, global struct periph_global_state *periph_state, global uint8_t *memory_ptr, unsigned int cycles
#ifndef __OPENCL_VERSION__
, unsigned int cpu_id
#endif
)
{
	private core_globals core_globals;
    #ifndef __OPENCL_VERSION__
	    memset(&core_globals, 0, sizeof(core_globals));
		core_globals.pe_id = cpu_id;
    #else
	    core_globals.pe_id = get_global_id(0);
    #endif


	core_globals.cpu = cpu_states[core_globals.pe_id];
	core_globals.periph_state = periph_state;

    #if defined(__OPENCL_VERSION__) && __OPENCL_VERSION < 200
	    core_globals.memory = memory_ptr;
    #else
	    memory = memory_ptr;
    #endif

	while(cycles--)
	{
		if((cycles & 7) == 0)
		{
			period_step_cpu(&core_globals);
			if(core_globals.pe_id == 0)
			   period_step_periph_global(&core_globals);
		}

		gicv3_process_interrupts(&core_globals);
		handle_pending_interrupts(&core_globals);

		bool success = true;
		uint32_t instruction = read_word(&core_globals, core_globals.cpu.pc, &success);
		if(!success)
		{
			handle_instruction_abort(&core_globals);
			continue;
		}

		if(!interpret_instruction(&core_globals, instruction))
		{
			core_globals.cpu.pc = 0;
			break;
		}

		if(core_globals.return_to_host)
			break;
	}

	cpu_states[core_globals.pe_id] = core_globals.cpu;
}

void init_cpu(struct cpu *cpu_state)
{
	cpu_state->current_el = 1;
	cpu_state->SPSel = 1;
	cpu_state->DAIF = 0b1111 << 6;
	cpu_state->CNTFRQ_EL0 = 1 * 1000 * 1000;
}
