#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120

#include <CL/cl2.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <fstream>

#include "cpudefs.h"
#include "mmio.h"

int main(void)
{
	setenv("OCL_STRICT_CONFORMANCE", "0", 1);

	// Filter for a 2.0 platform and set it as the default
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	auto plat = std::find_if(platforms.begin(), platforms.end(), [](cl::Platform &p){
	    return p.getInfo<CL_PLATFORM_VERSION>().find("OpenCL") != std::string::npos;
    });

	if (plat == platforms.end())  {
		std::cout << "No OpenCL platform found.";
		return -1;
	}

	cl::Platform newP = cl::Platform::setDefault(*plat);
	if (newP != *plat) {
		std::cout << "Error setting default platform.";
		return -1;
	}

	std::vector<std::string> sources = {
		"#define NO_FP_INTERPRETER 1\n",
		"#include \"interpreter.c\"\n",
		"#include \"mmu.c\"\n",
		"#include \"mmio.c\"\n",
		"#include \"cpu.c\"\n",
	};

	cl::Program program(sources);
	try
	{
		program.build("-cl-strict-aliasing -cl-mad-enable -cl-fast-relaxed-math -Werror");// -cl-std=CL2.0");
	}
	catch (...)
	{
		cl_int buildErr = CL_SUCCESS;
		for (auto &pair : program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&buildErr))
			std::cerr << pair.second << std::endl << std::endl;

		return 1;
	}

	std::vector<uint8_t> memory(1024*1024*64, 0);

	// Load the kernel
	FILE *kernelFile = fopen("Image-debug", "rb");

	// Read the header
	fread(memory.data(), sizeof(memory[0]), 32, kernelFile);

	size_t offset = *(uint64_t*)(memory.data()+8);
	size_t size = *(uint64_t*)(memory.data()+16);

	fseek(kernelFile, 0, SEEK_SET);
	fread(memory.data()+offset, size, 1, kernelFile);
	fclose(kernelFile);

	// Write secondary init code to memory
	uint32_t *secondary_init = reinterpret_cast<uint32_t*>(memory.data() + memory.size()/2);
	secondary_init[0] = 0xf94000a6; // ldr     x6, [x5]
	secondary_init[1] = 0xb4ffffe6; // cbz     x6, 0 <loop>
	secondary_init[2] = 0xd61f00c0; // br      x6

	FILE *dtbFile = fopen("device-tree.dtb", "rb");
	fread(memory.data(), 1, offset, dtbFile);
	fclose(dtbFile);

	std::vector<struct cpu> cpus{2};

	struct periph_global_state pgs;
	init_periph_global_state(&pgs, cpus.size());

	for(auto &&cpu : cpus)
		init_cpu(&cpu);

	// Set kernel args
	// Address of DTB
	cpus[0].reg[0] = 0x10000000;
	// Entry point
	cpus[0].pc = 0x10000000 + offset;

	// Set secondary CPU args
	for(unsigned int i = 1; i < cpus.size(); ++i)
	{
		// The spin table begins at the end of memory and grows down
		cpus[i].reg[5] = 0x10000000 + memory.size() - 8 * i;
		cpus[i].pc = 0x10000000 + reinterpret_cast<uint8_t*>(secondary_init) - memory.data();
	}

	cl::Buffer memoryBuffer(begin(memory), end(memory), false, true);
	cl::Buffer cpuBuffer(CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(struct cpu) * cpus.size(), cpus.data());
	cl::Buffer periphBuffer(CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(pgs), &pgs);

	auto interpreterKernel =
	    cl::KernelFunctor<decltype(cpuBuffer)&,
	        decltype(periphBuffer)&,
	        decltype(memoryBuffer)&,
	        unsigned int>(program, "interpret");

	cl_int error;
	for(int i = 100; --i;)
	{
		interpreterKernel(cl::EnqueueArgs(2), cpuBuffer, periphBuffer, memoryBuffer, 100000, error);
		cl::copy(cpuBuffer, cpus.begin(), cpus.end());
		if(std::any_of(cpus.begin(), cpus.end(), [](auto &c) { return c.pc == 0; }))
			break;
	}

	std::cout << std::hex << cpus[0].pc << std::endl;

	return 0;
}
