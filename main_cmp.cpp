#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120

#include <CL/cl2.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <fstream>

#include "cpudefs.h"

int main(void)
{
	setenv("OCL_STRICT_CONFORMANCE", "0", 1);

	// Filter for a 2.0 platform and set it as the default
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	auto plat = std::find_if(platforms.begin(), platforms.end(), [](cl::Platform &p){
	    return p.getInfo<CL_PLATFORM_VERSION>().find("OpenCL") != std::string::npos;
    });

	if (plat == platforms.end())  {
		std::cout << "No OpenCL platform found.";
		return -1;
	}

	cl::Platform newP = cl::Platform::setDefault(*plat);
	if (newP != *plat) {
		std::cout << "Error setting default platform.";
		return -1;
	}

	std::vector<std::string> sources;

	for(auto &&filename : {"interpreter.c", "mmu.c", "mmio.c"})
	{
		std::ifstream t(filename);
		t.seekg(0, std::ios::end);
		size_t size = t.tellg();
		std::string source(size, '\0');
		t.seekg(0);
		t.read(&source[0], size);
		sources.emplace_back(std::move(source));
	}

	cl::Program program(sources);
	try
	{
		program.build("-cl-strict-aliasing -cl-mad-enable -cl-fast-relaxed-math -Werror");// -cl-std=CL2.0");
	}
	catch (...)
	{
		cl_int buildErr = CL_SUCCESS;
		for (auto &pair : program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&buildErr))
			std::cerr << pair.second << std::endl << std::endl;

		return 1;
	}

	std::vector<uint8_t> memory(1024*1024*32, 0);

	auto cpuPointer = std::make_unique<struct cpu>();
	init_cpu(cpuPointer.get());

	// Load the kernel
	FILE *kernelFile = fopen("Image-debug", "rb");

	// Read the header
	fread(memory.data(), sizeof(memory[0]), 32, kernelFile);

	size_t offset = *(uint64_t*)(memory.data()+8);
	size_t size = *(uint64_t*)(memory.data()+16);

	fseek(kernelFile, 0, SEEK_SET);
	fread(memory.data()+offset, size, 1, kernelFile);
	fclose(kernelFile);

	FILE *dtbFile = fopen("device-tree.dtb", "rb");
	fread(memory.data(), 1, offset, dtbFile);
	fclose(dtbFile);

	// Set kernel args
	cpuPointer->reg[0] = 0x10000000;
	cpuPointer->pc = 0x10000000 + offset;

	struct periph_global_state pgs;
	init_periph_global_state(&pgs, 1);

	cl::Buffer memoryBuffer(begin(memory), end(memory), false, true);
	cl::Buffer cpuBuffer(CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(struct cpu), cpuPointer.get());
	cl::Buffer periphBuffer(CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(pgs), &pgs);

	auto interpreterKernel =
	    cl::KernelFunctor<decltype(cpuBuffer)&,
	        decltype(periphBuffer)&,
	        decltype(memoryBuffer)&,
	        unsigned int>(program, "interpret");

	struct cpu cpu(*cpuPointer);

	cl_int error;
	for(int i = 100000000; --i;)
	{
		printf("\r%d", 100000000-i);
		interpreterKernel(cl::EnqueueArgs(1), cpuBuffer, periphBuffer, memoryBuffer, 1, error);
		interpret(&cpu, &pgs, memory.data(), 1, 0);
		cl::copy(cpuBuffer, cpuPointer.get(), cpuPointer.get() + 1);
		if(memcmp(cpuPointer.get(), &cpu, sizeof(cpu)))
		{
			printf("\nCPU:\n");
			dump_cpu_state(&cpu);

			printf("\nGPU:\n");
			dump_cpu_state(cpuPointer.get());
			break;
		}
		if(cpuPointer->pc == 0)
			break;
	}

	std::cout << std::hex << cpuPointer->pc << std::endl;

	return 0;
}
