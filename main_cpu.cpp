#include <atomic>
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <fstream>
#include <thread>
#include <cstring>

#include <termios.h>
#include <unistd.h>

#include "cpudefs.h"
#include "mmio.h"

class TTYInputRAII {
private:
	struct termios old_termios;

public:
	TTYInputRAII() {
		tcgetattr(STDIN_FILENO, &old_termios);

		struct termios new_termios = old_termios;

		cfmakeraw(&new_termios);

		tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_termios);
	}

	~TTYInputRAII() {
		tcsetattr(STDIN_FILENO, TCSAFLUSH, &old_termios);
	}
};

int main(int argc, const char **argv)
{
	std::vector<uint8_t> memory(1024*1024*1024*1ul, 0);

	// Load the kernel
	if(argc == 1)
		argv[1] = "Image";

	FILE *kernelFile = fopen(argv[1], "rb");

	// Read the header
	fread(memory.data(), sizeof(memory[0]), 32, kernelFile);

	size_t kernel_offset = *(uint64_t*)(memory.data()+8);
	const size_t kernel_size = *(uint64_t*)(memory.data()+16);

	if(kernel_offset == 0)
		kernel_offset = 0x200000;

	FILE *dtbFile = fopen("device-tree.dtb", "rb");
	size_t size = fread(memory.data(), 1, memory.size(), dtbFile);
	if(size > kernel_offset) exit(1); // DTB too big
	printf("DTB: %lx - %lx\n", 0x10000000ul, 0x10000000+size);
	fclose(dtbFile);

	fseek(kernelFile, 0, SEEK_SET);
	size = fread(memory.data()+kernel_offset, 1, kernel_size, kernelFile);
	printf("Kernel: %lx - %lx\n", 0x10000000 + kernel_offset, 0x10000000+kernel_offset+size);
	fclose(kernelFile);

	// Write secondary init code to memory
	uint32_t *secondary_init = reinterpret_cast<uint32_t*>(memory.data() + kernel_offset - 3*sizeof(uint32_t));
	secondary_init[0] = 0xf94000a6; // ldr     x6, [x5]
	secondary_init[1] = 0xb4ffffe6; // cbz     x6, 0 <loop>
	secondary_init[2] = 0xd61f00c0; // br      x6

	FILE *initrdFile = fopen("buildroot.cpio", "rb");
	//FILE *initrdFile = fopen("initramfs", "rb");
	size = fread(memory.data() + 0x2000000, 1, memory.size() - 0x2000000, initrdFile);
	printf("Initrd: %lx - %lx\n", 0x10000000ul + 0x2000000, 0x10000000+0x2000000+size);
	fclose(initrdFile);

	std::vector<struct cpu> cpus{1};

	struct periph_global_state pgs;
	init_periph_global_state(&pgs, cpus.size());

	for(auto &&cpu : cpus)
		init_cpu(&cpu);

	// Set kernel args
	// Address of DTB
	cpus[0].reg[0] = 0x10000000;
	// Entry point
	cpus[0].pc = 0x10000000 + kernel_offset;

	// Set secondary CPU args
	for(unsigned int i = 1; i < cpus.size(); ++i)
	{
		// The spin table begins at the end of memory and grows down
		cpus[i].reg[5] = 0x10000000 + memory.size() - 8 * i;
		cpus[i].pc = 0x10000000 + reinterpret_cast<uint8_t*>(secondary_init) - memory.data();
	}

	std::atomic_bool running = true;

	std::vector<std::thread> cpu_threads;

	TTYInputRAII tty_input_raii;

	// Spin up CPUs
	for(unsigned int i = 0; i < cpus.size(); ++i)
	{
		cpu_threads.emplace_back([&cpus,&pgs,&memory,&running,i] {
			while(running.load())
			{
				interpret(cpus.data(), &pgs, memory.data(), 128000, i);

				pl011_flush_output(&pgs);
				pl011_fill_input(&pgs);

				if(cpus[i].pc == 0)
					running.store(false);
			}
		});
	}

	// Wait for execution to stop
	for(auto &&thread : cpu_threads)
		thread.join();

	std::cout << std::hex << cpus[0].reg[0] << std::endl;

	return 0;
}
