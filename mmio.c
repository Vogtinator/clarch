#ifndef __OPENCL_VERSION__
#include <assert.h>
#include <poll.h>
#include <unistd.h>
#endif

#include "core_globals.h"
#include "mmio.h"

static uint32_t gicv3_dist_read_word(core_globals *globals, uint64_t address)
{
	uint16_t offset = address & 0xFFFF;
	uint8_t n = (address & 0x7F) >> 2;
	switch(offset)
	{
	case 0x0000: // GICD_CTLR
		return globals->periph_state->gic.GICD_CTRLR;
	case 0x0004: // GICD_TYPER
		return 15 << 19 | 1; // GICv3, 64 IRQs (16 SGI, 16 PPI, 32 SPI)
	case 0x0104: // GICD_ISENABLERn
		return globals->periph_state->gic.GICD_IENABLER[n];
	case 0x0C08: case 0x0C0C: // GICD_ICFGRn
		return globals->periph_state->gic.GICD_ICFGR[n];
	case 0xFFE8: // GICD_PIDR2
		return 0x30; // GICv3r0
	}

	printf("Unknown GICv3 dist read: %lx\n", address);
	return 0;
}

static uint32_t gicv3_rdist_read_word(core_globals *globals, uint64_t address)
{
	// One redistributor per PE
	uint8_t pe_id = (address - 0x1000000) / 0x20000;

	uint32_t offset = address & 0x1FFFF;
	switch(offset)
	{
	// RD_BASE
	case 0x00000: // GICR_CTRLR
		return globals->periph_state->per_cpu[pe_id].gic.GICR_CTRLR;
	case 0x00014: // GICR_WAKER
		return 0;
	case 0x0FFE8: // GICR_PIDR2
		return 0x30; // GICv3r0

	// SGI_BASE
	case 0x10c04: // GICR_ICFGR1
		return globals->periph_state->per_cpu[pe_id].gic.GICR_ICFGR1;
	}

	printf("Unknown GICv3 rdist read: %lx\n", address);
	return 0;
}

static uint64_t gicv3_rdist_read_dword(core_globals *globals, uint64_t address)
{
	// One redistributor per PE
	uint8_t pe_id = (address - 0x1000000) / 0x20000;

	uint32_t offset = address & 0x1FFFF;
	switch(offset)
	{
	case 0x00008: // GICR_TYPER
		if(pe_id == globals->periph_state->count_pes - 1)
			return (uint64_t)pe_id << 32 | pe_id << 8 | 1 << 4; // Last redistributor
		return (uint64_t)pe_id << 32 | pe_id << 8;
	}

	printf("Unknown GICv3 rdist read: %lx\n", address);
	return 0;
}

static void gicv3_dist_write_word(core_globals *globals, uint64_t address, uint32_t value)
{
	uint8_t n = (address & 0x7F) >> 2;

	uint16_t offset = address & 0xFFFF;
	switch(offset)
	{
	case 0x0000: // GICD_CTRLR
		globals->periph_state->gic.GICD_CTRLR = value;
		return;
	case 0x0084: // GICD_IGROUPRn
		globals->periph_state->gic.GICD_IGROUPR[n] = value;
		return;
	case 0x0104: // GICD_ISENABLERn
		globals->periph_state->gic.GICD_IENABLER[n] |= value;
		return;
	case 0x0184: // GICD_ICENABLERn
		globals->periph_state->gic.GICD_IENABLER[n] &= ~value;
		return;
	case 0x0304: // GICD_ISACTIVERn
		globals->periph_state->gic.GICD_IACTIVER[n] |= value;
		return;
	case 0x0384: // GICD_ICACTIVERn
		globals->periph_state->gic.GICD_IACTIVER[n] &= ~value;
		return;
	case 0x0420: case 0x0424: case 0x0428: case 0x042C: // GICD_IPRIORITYRn
	case 0x0430: case 0x0434: case 0x0438: case 0x043C:
		globals->periph_state->gic.GICD_IPRIORITYR[n] = value;
		return;
	case 0x0C08: case 0x0C0C: // GICD_ICFGRn
		globals->periph_state->gic.GICD_ICFGR[n] = value;
		return;
	}

	printf("Unknown GICv3 dist write: %lx %x\n", address, value);
	return;
}

static void gicv3_dist_write_dword(core_globals *globals, uint64_t address, uint64_t value)
{
	(void) globals;
	(void) value;

	uint16_t offset = address & 0xFFFF;

	// Ignore GICD_IROUTERn, not implemented
	if(offset >= 0x6100 && offset < 0x6200)
		return;

	printf("Unknown GICv3 dist read: %lx\n", address);
	return;
}

static void gicv3_rdist_write_word(core_globals *globals, uint64_t address, uint32_t value)
{
	// One redistributor per PE
	uint8_t pe_id = (address - 0x1000000) / 0x20000;

	uint32_t offset = address & 0x1FFFF;
	switch(offset)
	{
	// RD_BASE
	case 0x00014: // GICR_WAKER
		return;
	// SGI_BASE
	case 0x10080: // GICR_IGROUPR0
		globals->periph_state->per_cpu[pe_id].gic.GICR_IGROUPR0 = value;
		return;
	case 0x10100: // GICR_ISENABLER0
		globals->periph_state->per_cpu[pe_id].gic.GICR_IENABLER0 |= value;
		return;
	case 0x10180: // GICR_ICENABLER0
		globals->periph_state->per_cpu[pe_id].gic.GICR_IENABLER0 &= ~value;
		return;
	case 0x10300: // GICR_ISACTIVE0
		globals->periph_state->per_cpu[pe_id].gic.GICR_IACTIVE0 |= value;
		return;
	case 0x10380: // GICR_ICACTIVE0
		globals->periph_state->per_cpu[pe_id].gic.GICR_IACTIVE0 &= ~value;
		return;
	case 0x10400: case 0x10404: case 0x10408: case 0x1040c:
	case 0x10410: case 0x10414: case 0x10418: case 0x1041c: // GICR_IPRIORITYRn
		globals->periph_state->per_cpu[pe_id].gic.GICR_IPRIORITYR[(offset - 0x10400)/4] = value;
		return;
	case 0x10c04: // GICR_ICFGR1
		globals->periph_state->per_cpu[pe_id].gic.GICR_ICFGR1 = value;
		return;
	}

	printf("Unknown GICv3 rdist write: %lx %x\n", address, value);
}

void pl011_int_check(struct core_globals *globals)
{
	gicv3_set_spi(globals, 0, globals->periph_state->pl011.int_status & globals->periph_state->pl011.int_mask);
}

uint32_t pl011_read_word(struct core_globals *globals, uint64_t address)
{
	switch (address & 0xFFFF) {
	    case 0x000:
	    {
		    if(globals->periph_state->pl011.input_queue_next == globals->periph_state->pl011.input_queue_end)
				return 0;

			uint8_t in = globals->periph_state->pl011.input_queue[globals->periph_state->pl011.input_queue_next];

			globals->periph_state->pl011.input_queue_next += 1;

			// More coming?
			if(globals->periph_state->pl011.input_queue_next != globals->periph_state->pl011.input_queue_end)
				return in;

			// If not, clear the RX flag
			globals->periph_state->pl011.rx = 0;
			globals->periph_state->pl011.int_status &= ~0x10;
			pl011_int_check(globals);

			return in;
	    }
	    case 0x018: return 0x90 & ~(globals->periph_state->pl011.rx << 4);
	    case 0x024: return 0;
	    case 0x028: return 0;
	    case 0x02C: return 0;
	    case 0x030: return globals->periph_state->pl011.cr;
	    case 0x038: return globals->periph_state->pl011.int_mask;
	    case 0x03C: return globals->periph_state->pl011.int_status;
	    case 0x040: return globals->periph_state->pl011.int_status & globals->periph_state->pl011.int_mask;
	    case 0xFE0: return 0x11;
	    case 0xFE4: return 0x10;
	    case 0xFE8: return 0x14;
	    case 0xFEC: return 0x00;
	    case 0xFF0: return 0x0d;
	    case 0xFF4: return 0xf0;
	    case 0xFF8: return 0x05;
	    case 0xFFC: return 0xb1;
	}

	printf("Unknown PL011 read: %lx\n", address);
	return 0;
}

void pl011_write_word(struct core_globals *globals, uint64_t address, uint32_t value)
{
	switch (address & 0xFFFF) {
	    case 0x000:
		    if(globals->periph_state->pl011.output_queue_next == sizeof(globals->periph_state->pl011.output_queue) - 1)
				globals->return_to_host = true;

			assert(globals->periph_state->pl011.output_queue_next < sizeof(globals->periph_state->pl011.output_queue));

			globals->periph_state->pl011.output_queue[globals->periph_state->pl011.output_queue_next] = value;
			globals->periph_state->pl011.output_queue_next += 1;
		    return;
	    case 0x004: return;
	    case 0x024: return;
	    case 0x028: return;
	    case 0x02C: return;
	    case 0x030: globals->periph_state->pl011.cr = value; return;
	    case 0x034: return;
	    case 0x038:
		    globals->periph_state->pl011.int_mask = value & 0x7FF;
			pl011_int_check(globals);
		    return;
	    case 0x044:
		    globals->periph_state->pl011.int_status &= ~value;
			pl011_int_check(globals);
		    return;
	}
}

void pl011_step(struct core_globals *globals)
{
	// If there is already data waiting to be read, don't do anything
	if(globals->periph_state->pl011.input_queue_next == globals->periph_state->pl011.input_queue_end)
		return;

	globals->periph_state->pl011.int_status |= 0x10;
	globals->periph_state->pl011.rx = 1;
	pl011_int_check(globals);
}

#ifndef __OPENCL_VERSION__
void pl011_flush_output(struct periph_global_state *periph_state)
{
	// Buffer empty?
	if(periph_state->pl011.output_queue_next == 0)
		return;

	fwrite(periph_state->pl011.output_queue, 1, periph_state->pl011.output_queue_next, stdout);
	fflush(stdout);

	periph_state->pl011.output_queue_next = 0;
}

void pl011_fill_input(struct periph_global_state *periph_state)
{
	struct pl011_state *state = &periph_state->pl011;

	// Readjust input buffer to the left
	state->input_queue_end -= state->input_queue_next;
	memmove(state->input_queue, state->input_queue + state->input_queue_next, state->input_queue_end);
	state->input_queue_next = 0;

	int space = sizeof(state->input_queue) - state->input_queue_end;

	// Buffer full?
	if(space == 0)
		return;

	struct pollfd pfd;
	pfd.fd = 0;
	pfd.events = POLLIN;

	if(poll(&pfd, 1, 0) > 0)
	{
		space = read(0, state->input_queue + state->input_queue_end, space);

		if(space > 0)
			state->input_queue_end += space;
	}
}
#endif

uint64_t mmio_read_dword(struct core_globals *globals, uint64_t address)
{
	switch(address >> 24)
	{
	case 1: case 2:
		return gicv3_rdist_read_dword(globals, address);
	}

	printf("Unknown read: %lx\n", address);
	return 0;
}

uint32_t mmio_read_word (struct core_globals *globals, uint64_t address)
{
	switch(address >> 24)
	{
	case 0:
		return gicv3_dist_read_word(globals, address);
	case 1: case 2:
		return gicv3_rdist_read_word(globals, address);
	case 3:
		return pl011_read_word(globals, address);
	}

	printf("Unknown read: %lx\n", address);
	return 0;
}

uint16_t mmio_read_half(struct core_globals *globals, uint64_t address)
{
	switch(address >> 24)
	{
	case 3:
		return pl011_read_word(globals, address);
	}

	printf("Unknown read: %lx\n", address);
	return 0;
}

uint8_t  mmio_read_byte (struct core_globals *globals, uint64_t address)
{printf("BR %lx\n", address);
	return 0;
}

void mmio_write_dword(struct core_globals *globals, uint64_t address, uint64_t value)
{
	switch(address >> 24)
	{
	case 0:
		return gicv3_dist_write_dword(globals, address, value);
	}

	printf("Unknown write: %lx %lx\n", address, value);
}

void mmio_write_word (struct core_globals *globals, uint64_t address, uint32_t value)
{
	switch(address >> 24)
	{
	case 0:
		return gicv3_dist_write_word(globals, address, value);
	case 1: case 2:
		return gicv3_rdist_write_word(globals, address, value);
	case 3:
		return pl011_write_word(globals, address, value);
	}

	printf("Unknown write: %lx %x\n", address, value);
}

void mmio_write_half(struct core_globals *globals, uint64_t address, uint16_t value)
{
	switch(address >> 24)
	{
	case 3:
		return pl011_write_word(globals, address, value);
	}

	printf("Unknown write: %lx %x\n", address, value);
}

void mmio_write_byte (struct core_globals *globals, uint64_t address, uint8_t  value)
{
	switch(address >> 24)
	{
	case 3:
		return pl011_write_word(globals, address, value);
	}

	printf("Unknown write: %lx %x\n", address, value);
}

void init_periph_local_state(struct periph_local_state *periph_state)
{
	memset(periph_state, 0, sizeof(*periph_state));
}

void init_periph_global_state(struct periph_global_state *periph_state, unsigned int count_pes)
{
	memset(periph_state, 0, sizeof(*periph_state));

	periph_state->gic.GICD_CTRLR = 1 << 6;
	periph_state->pl011.cr = 0x300;
	periph_state->count_pes = count_pes;

	for(unsigned int i = 0; i < MAX_CPUS; ++i)
		init_periph_local_state(&periph_state->per_cpu[i]);
}

void gicv3_process_interrupts(core_globals *globals)
{
	// Priorities are ignored here for simplicity
	uint32_t pending = globals->periph_state->per_cpu[globals->pe_id].gic.GICR_IPENDR0 & globals->periph_state->per_cpu[globals->pe_id].gic.GICR_IENABLER0;

	if(pending && (globals->cpu.ICC_IGRPEN1_EL1 & 1))
	{
		uint8_t intid = ctz32(pending);

		globals->cpu.ICC_IAR1_EL1 = intid;
		globals->cpu.irq_pending = true;
		return;
	}

	// No routing whatsoever - just forward all SPIs to PE 0
	if(globals->pe_id == 0)
	{
		uint8_t intid = 32;

		for(unsigned int i = 1; i < sizeof(globals->periph_state->gic.GICD_IPENDR)/4; ++i, intid += 32)
		{
			pending = globals->periph_state->gic.GICD_IPENDR[i] & globals->periph_state->gic.GICD_IENABLER[i];

			if(!pending)
				continue;

			globals->cpu.ICC_IAR1_EL1 = intid + ctz32(pending);
			globals->cpu.irq_pending = true;
			return;
		}
	}

	globals->cpu.ICC_IAR1_EL1 = 0x3ff;
	globals->cpu.irq_pending = false;
}

void gicv3_set_ppi(core_globals *globals, uint8_t nr, bool state)
{
	uint8_t intid = nr + 16;

	if(state)
		globals->periph_state->per_cpu[globals->pe_id].gic.GICR_IPENDR0 |= 1u << intid;
	else
		globals->periph_state->per_cpu[globals->pe_id].gic.GICR_IPENDR0 &= ~(1u << intid);
}

void gicv3_ack_interrupt(core_globals *globals, uint32_t nr)
{
	// TODO: Latch interrupts instead of calling givc3_set_*(..., false);

	if(nr < 16) // SGI
		assert(false||1);
	else if(nr < 32) // PPI
		gicv3_set_ppi(globals, nr - 16, false);
	else if(nr < 1020) // SPI or LPI
		gicv3_set_spi(globals, nr - 32, false);
}

void gicv3_set_spi(core_globals *globals, uint8_t nr, bool state)
{
	uint8_t intid = nr + 32;

	if(state)
		globals->periph_state->gic.GICD_IPENDR[intid / 32] |= 1u << (intid & 31);
	else
		globals->periph_state->gic.GICD_IPENDR[intid / 32] &= ~(1 << (intid & 31));
}

void period_step_periph_global(core_globals *globals)
{
	pl011_step(globals);
}
