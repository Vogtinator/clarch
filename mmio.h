#pragma once

#include "opencl_compat.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_CPUS 16

struct gicv3_local_state {
	uint32_t GICR_CTRLR;
	uint32_t GICR_IGROUPR0;
	uint32_t GICR_IACTIVE0;
	uint32_t GICR_IENABLER0;
	uint32_t GICR_IPRIORITYR[8];
	uint32_t GICR_ICFGR1;
	uint32_t GICR_IPENDR0;
};

struct gicv3_global_state {
	uint32_t GICD_CTRLR;
	uint32_t GICD_IGROUPR[2];
	uint32_t GICD_IENABLER[2];
	uint32_t GICD_IPENDR[2];
	uint32_t GICD_IACTIVER[2];
	uint32_t GICD_ICFGR[4];
	uint32_t GICD_IPRIORITYR[16];
};

struct pl011_state {
	uint32_t int_status;
	uint32_t int_mask;
	uint32_t cr;
	uint32_t rx;
	uint8_t input_queue[16]; // Right-aligned
	uint8_t input_queue_next;
	uint8_t input_queue_end;
	uint8_t output_queue[128]; // Left-aligned
	uint8_t output_queue_next;
};

struct periph_local_state {
	struct gicv3_local_state gic;
};

// State shared between all cores - make sure to synchronize access.
struct periph_global_state {
	struct gicv3_global_state gic;
	struct pl011_state pl011;
	unsigned int count_pes;
	struct periph_local_state per_cpu[MAX_CPUS];
};

struct core_globals;

uint64_t mmio_read_dword(struct core_globals *globals, uint64_t address);
uint32_t mmio_read_word (struct core_globals *globals, uint64_t address);
uint16_t mmio_read_half (struct core_globals *globals, uint64_t address);
uint8_t  mmio_read_byte (struct core_globals *globals, uint64_t address);

void mmio_write_dword(struct core_globals *globals, uint64_t address, uint64_t value);
void mmio_write_word (struct core_globals *globals, uint64_t address, uint32_t value);
void mmio_write_half (struct core_globals *globals, uint64_t address, uint16_t value);
void mmio_write_byte (struct core_globals *globals, uint64_t address, uint8_t  value);

// Called by peripherals
void gicv3_set_spi(struct core_globals *globals, uint8_t nr, bool state);
void gicv3_set_ppi(struct core_globals *globals, uint8_t nr, bool state);

// Called by the CPU
void gicv3_process_interrupts(struct core_globals *globals);
void gicv3_ack_interrupt(struct core_globals *globals, uint32_t nr);
void period_step_periph_global(struct core_globals *globals);

// Called by the host
void pl011_flush_output(struct periph_global_state *periph_state);
void pl011_fill_input(struct periph_global_state *periph_state);

void init_periph_global_state(struct periph_global_state *periph_state, unsigned int count_pes);

#ifdef __cplusplus
}
#endif
