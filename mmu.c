#include "core_globals.h"
#include "mmio.h"

#if !(defined(__OPENCL_VERSION__) && __OPENCL_VERSION < 200)
	global uint8_t *memory;
#endif

global void *phys_ptr(core_globals *globals, uint64_t address)
{
	// MMIO?
	if(address < 0x10000000)
		return 0;

	if(address - 0x10000000 >= 1024*1024*1024*1ul)
		return 0;

    #if defined(__OPENCL_VERSION__) && __OPENCL_VERSION < 200
	    return globals->memory + (address - 0x10000000);
    #else
	    (void) globals;
	    return memory + (address - 0x10000000);
    #endif
}

static bool has_write_access(uint8_t el, uint64_t pte)
{
	return el ? !(pte & 0b10000000) : ((pte & 0b11000000) == 0b01000000);
}

static bool has_read_access(uint8_t el, uint64_t pte)
{
	return (el > 0) || (pte & 0b01000000);
}

uint64_t translate_address(core_globals *globals, uint64_t address, bool write, bool *success)
{
	if((globals->cpu.SCTLR_EL1 & 1) == 0)
		return address;

	// Find the TTBR to use
	uint64_t ttbr;

	uint8_t t0sz = globals->cpu.TCR_EL1 & 0x3F,
	        t1sz = (globals->cpu.TCR_EL1 >> 16) & 0x3F;

	uint64_t t1_bottom = ~0ul << (64 - t1sz),
	         t0_top = 1ul << (64 - t0sz);

	uint8_t va_bits = 0;

	if(address >= t1_bottom)
	{
		ttbr = globals->cpu.TTBR1_EL1;
		va_bits = 64 - t1sz;
	}
	else if(address < t0_top)
	{
		ttbr = globals->cpu.TTBR0_EL1;
		va_bits = 64 - t0sz;
	}
	else
	{
		globals->cpu.ESR_EL[1-1] = 0b000100;
		globals->cpu.FAR_EL[1-1] = address;
		*success = false;
		return 0;
	}

	uint64_t descriptor = ttbr;
	global uint64_t *ttb;

	// With VA > 39 bits, a level 0 lookup is needed
	if(va_bits > 39)
	{
		ttb = (global uint64_t*) phys_ptr(globals, descriptor & 0x0000FFFFFFFFF000ul);
		if(!ttb)
		{
			printf("Not implemented\n");
			*success = false;
			return 0;
		}
		descriptor = ttb[(address >> 39) & 0x1FF];

		if((descriptor & 3) != 0b11)
		{
			globals->cpu.ESR_EL[1-1] = 0b000100;
			globals->cpu.FAR_EL[1-1] = address;
			*success = false;
			return 0;
		}
	}

	// With VA > 30 bits, a level 1 lookup is needed
	if(va_bits > 30)
	{
		ttb = (global uint64_t*) phys_ptr(globals, descriptor & 0x0000FFFFFFFFF000ul);
		if(!ttb)
		{
			printf("Not implemented\n");
			*success = false;
			return 0;
		}
		descriptor = ttb[(address >> 30) & 0x1FF];

		switch(descriptor & 3)
		{
		case 0b00:
		case 0b10: // Invalid
			globals->cpu.ESR_EL[1-1] = 0b000101;
			globals->cpu.FAR_EL[1-1] = address;
			*success = false;
			return 0;
		case 0b01: // Block
			if((write && !has_write_access(globals->cpu.current_el, descriptor))
			   || (!write && !has_read_access(globals->cpu.current_el, descriptor)))
			{
				globals->cpu.ESR_EL[1-1] = 0b001101;
				globals->cpu.FAR_EL[1-1] = address;
				*success = false;
				return 0;
			}
			if(!(descriptor & (1 << 10)))
			{
				globals->cpu.ESR_EL[1-1] = 0b001001;
				globals->cpu.FAR_EL[1-1] = address;
				*success = false;
				return 0;
			}

			return (address & 0x00003FFFFFFFul) | (descriptor & 0xFFFFC0000000ul);
		}
	}

	ttb = (global uint64_t*) phys_ptr(globals, descriptor & 0x0000FFFFFFFFF000ul);
	if(!ttb)
	{
		printf("Not implemented\n");
		*success = false;
		return 0;
	}
	descriptor = ttb[(address >> 21) & 0x1FF];

	switch(descriptor & 3)
	{
	case 0b00:
	case 0b10: // Invalid
		globals->cpu.ESR_EL[1-1] = 0b000110;
		globals->cpu.FAR_EL[1-1] = address;
		*success = false;
		return 0;
	case 0b01: // Block
		if((write && !has_write_access(globals->cpu.current_el, descriptor))
		   || (!write && !has_read_access(globals->cpu.current_el, descriptor)))
		{
			globals->cpu.ESR_EL[1-1] = 0b001110;
			globals->cpu.FAR_EL[1-1] = address;
			*success = false;
			return 0;
		}
		if(!(descriptor & (1 << 10)))
		{
			globals->cpu.ESR_EL[1-1] = 0b001010;
			globals->cpu.FAR_EL[1-1] = address;
			*success = false;
			return 0;
		}
		return (address & 0x1FFFFFul) | (descriptor & 0xFFFFFFE00000ul);
	}

	ttb = (global uint64_t*) phys_ptr(globals, descriptor & 0x0000FFFFFFFFF000ul);
	if(!ttb)
	{
		printf("Not implemented\n");
		*success = false;
		return 0;
	}
	descriptor = ttb[(address >> 12) & 0x1FF];

	// Only pages permitted at this level
	if((descriptor & 3) != 0b11)
	{
		globals->cpu.ESR_EL[1-1] = 0b000111;
		globals->cpu.FAR_EL[1-1] = address;
		*success = false;
		return 0;
	}

	if((write && !has_write_access(globals->cpu.current_el, descriptor))
	   || (!write && !has_read_access(globals->cpu.current_el, descriptor)))
	{
		globals->cpu.ESR_EL[1-1] = 0b001111;
		globals->cpu.FAR_EL[1-1] = address;
		*success = false;
		return 0;
	}

	return (address & 0xFFFul) | (descriptor & 0xFFFFFFFFF000ul);
}

static uintptr_t addr_cache_value(core_globals *globals, uint64_t address, bool write)
{
	uint64_t page = address & ~0xFFFul,
	         key = page | write;
	uintptr_t entry = 0;

    #ifdef __OPENCL_VERSION__
	    for(int i = 0; !entry && i < addr_cache_size / 8; ++i)
		{
			/* select does not exist for ulongN, so we use the result of the comparison
			 * as mask and and the values, then use the sum. */
			ulong8 matches8 = as_ulong8(globals->addr_cache.entries[i].keys == key);
			matches8 &= globals->addr_cache.entries[i].pointers;
			ulong4 matches4 = matches8.lo + matches8.hi;
			ulong2 matches2 = matches4.lo + matches4.hi;
			entry = matches2.lo + matches2.hi;
		}
    #else
	    for(int i = 0; !entry && i < addr_cache_size; ++i)
		{
			if(globals->addr_cache.entries[i].key == key)
				entry = globals->addr_cache.entries[i].pointer;
			i++;
			if(globals->addr_cache.entries[i].key == key)
				entry = globals->addr_cache.entries[i].pointer;
			i++;
			if(globals->addr_cache.entries[i].key == key)
				entry = globals->addr_cache.entries[i].pointer;
			i++;
			if(globals->addr_cache.entries[i].key == key)
				entry = globals->addr_cache.entries[i].pointer;
		}
    #endif

	if(entry)
		return entry;

	bool success = true;
	uint64_t phys_addr = translate_address(globals, address, write, &success);
	if(!success)
	{
		/* Debugging output
#ifndef __OPENCL_VERSION__
		fprintf(stderr, "Fault: %lx at (%lx -> %lx)\n", address, globals->cpu.pc, translate_address(globals, globals->cpu.pc, false, &success));

		dump_cpu_state(&globals->cpu);

		uint32_t insns[5];
		success = true;
		read_bytes(globals, globals->cpu.pc - 2*4, (uint8_t*)&insns, sizeof(insns), &success);
		printf("%x %x (%x) %x %x\n", insns[0], insns[1], insns[2], insns[3], insns[4]);
		print_backtrace(globals);
#endif*/
		return 0;
	}

	entry = (uintptr_t) phys_ptr(globals, phys_addr);
	if(!entry)
		entry = phys_addr + 1;

	entry -= address;

	unsigned int index = globals->addr_cache.index;

    #ifdef __OPENCL_VERSION__
	    globals->addr_cache.entries[index / 8].keys[index % 8] = key;
		globals->addr_cache.entries[index / 8].pointers[index % 8] = entry;
    #else
		globals->addr_cache.entries[index].key = key;
		globals->addr_cache.entries[index].pointer = entry;
    #endif

	globals->addr_cache.index = (globals->addr_cache.index + 1) % addr_cache_size;

	return entry;
}

void read_bytes(core_globals *globals, uint64_t address, uint8_t *dest, uint8_t count, bool *success)
{
	while(*success && count--)
		*dest++ = read_byte(globals, address++, success);
}

void write_bytes(core_globals *globals, uint64_t address, const uint8_t *src, uint8_t count, bool *success)
{
	while(*success && count--)
		write_byte(globals, address++, *src++, success);
}

uint64_t read_dword(core_globals *globals, uint64_t address, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, false);
	if(!entry) {
		*success = false;
		return 0;
	}

	if(entry & 1)
		return mmio_read_dword(globals, entry + address - 1);
	else if(address & 0b111)
	{
		uint64_t ret;
		read_bytes(globals, address, (uint8_t*)&ret, sizeof(ret), success);
		return ret;
	}
	else
		return *(global uint64_t*)(entry + address);
}

uint32_t read_word(core_globals *globals, uint64_t address, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, false);
	if(!entry) {
		*success = false;
		return 0;
	}

	if(entry & 1)
		return mmio_read_word(globals, entry + address - 1);
	else if(address & 0b11)
	{
		uint32_t ret;
		read_bytes(globals, address, (uint8_t*)&ret, sizeof(ret), success);
		return ret;
	}
	else
		return *(global uint32_t*)(entry + address);
}

uint16_t read_half(core_globals *globals, uint64_t address, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, false);
	if(!entry) {
		*success = false;
		return 0;
	}

	if(entry & 1)
		return mmio_read_half(globals, entry + address - 1);
	else if(address & 0b1)
	{
		uint16_t ret;
		read_bytes(globals, address, (uint8_t*)&ret, sizeof(ret), success);
		return ret;
	}
	else
		return *(global uint16_t*)(entry + address);
}

uint8_t read_byte(core_globals *globals, uint64_t address, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, false);
	if(!entry) {
		*success = false;
		return 0;
	}

	if(entry & 1)
		return mmio_read_byte(globals, entry + address - 1);
	else
		return *(global uint8_t*)(entry + address);
}

void write_dword(core_globals *globals, uint64_t address, uint64_t value, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, true);
	if(!entry) {
		*success = false;
		return;
	}

	if(entry & 1)
		return mmio_write_dword(globals, entry + address - 1, value);
	else if(address & 0b111)
		return write_bytes(globals, address, (const uint8_t*) &value, sizeof(value), success);
	else
		*(global uint64_t*)(entry + address) = value;
}

void write_word(core_globals *globals, uint64_t address, uint32_t value, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, true);
	if(!entry) {
		*success = false;
		return;
	}

	if(entry & 1)
		return mmio_write_word(globals, entry + address - 1, value);
	else if(address & 0b11)
		return write_bytes(globals, address, (const uint8_t*) &value, sizeof(value), success);
	else
		*(global uint32_t*)(entry + address) = value;
}

void write_half(core_globals *globals, uint64_t address, uint16_t value, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, true);
	if(!entry) {
		*success = false;
		return;
	}

	if(entry & 1)
		return mmio_write_half(globals, entry + address - 1, value);
	else if(address & 0b1)
		return write_bytes(globals, address, (const uint8_t*) &value, sizeof(value), success);
	else
		*(global uint16_t*)(entry + address) = value;
}

void write_byte(core_globals *globals, uint64_t address, uint8_t value, bool *success)
{
	uintptr_t entry = addr_cache_value(globals, address, true);
	if(!entry) {
		*success = false;
		return;
	}

	if(entry & 1)
		return mmio_write_byte(globals, entry + address - 1, value);
	else
		*(global uint8_t*)(entry + address) = value;
}

global void *virt_ptr(core_globals *globals, uint64_t address, bool write)
{
	uintptr_t entry = addr_cache_value(globals, address, write);
	if(!entry || entry & 1)
		return NULL;

	return (global void*)(entry + address);
}

void flush_addr_cache(core_globals *globals)
{
#ifdef __OPENCL_VERSION__
	for(int i = 0; i < addr_cache_size / 8; ++i)
		globals->addr_cache.entries[i].pointers = (ulong8)(0, 0, 0, 0, 0, 0, 0, 0);
#else
	for(int i = 0; i < addr_cache_size; ++i)
		globals->addr_cache.entries[i].pointer = 0;
#endif
}
