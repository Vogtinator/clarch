#pragma once

#include "opencl_compat.h"

#define addr_cache_size 16

/* If 0, invalid.
 * If bit 0 is 0, add the address to get a pointer to memory.
 * Else, add the address-1 to get the physical address to MMIO. */
struct addr_cache_entry {
	/* Different vectorized format for OpenCL. */
 	#ifdef __OPENCL_VERSION__
		ulong8 keys;
		ulong8 pointers;
	#else
		uint64_t key;
		uintptr_t pointer;
	#endif
};

typedef struct addr_cache {
	struct addr_cache_entry entries[addr_cache_size];
	unsigned int index;
} addr_cache;

struct core_globals;

uint64_t translate_address(struct core_globals *globals, uint64_t address, bool write, bool *success);
void flush_addr_cache(struct core_globals *globals);

// Returns NULL if not inside memory address space
global void *phys_ptr(struct core_globals *globals, uint64_t address);
// Returns NULL if translation fails, permission check fails or target not inside memory address space.
// NOTE: Does not handle page-crossing access!
global void *virt_ptr(struct core_globals *globals, uint64_t address, bool write);

/* For unaligned, potentially page-crossing reads.
 * Does not handle MMIO. */
void read_bytes(struct core_globals *globals, uint64_t address, uint8_t *dest, uint8_t count, bool *success);
/* For unaligned, potentially page-crossing writes.
 * Does not handle MMIO. */
void write_bytes(struct core_globals *globals, uint64_t address, const uint8_t *src, uint8_t count, bool *success);

// Pass a pointer to a true boolean for success, you need to check the return value
// to handle aborts etc.
uint64_t read_dword(struct core_globals *globals, uint64_t address, bool *success);
uint32_t read_word (struct core_globals *globals, uint64_t address, bool *success);
uint16_t read_half (struct core_globals *globals, uint64_t address, bool *success);
uint8_t  read_byte (struct core_globals *globals, uint64_t address, bool *success);

void write_dword(struct core_globals *globals, uint64_t address, uint64_t value, bool *success);
void write_word (struct core_globals *globals, uint64_t address, uint32_t value, bool *success);
void write_half (struct core_globals *globals, uint64_t address, uint16_t value, bool *success);
void write_byte (struct core_globals *globals, uint64_t address, uint8_t  value, bool *success);
