#pragma once

#if !defined(__OPENCL_VERSION__)
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#define global
#define kernel
#ifndef __cplusplus
    #define private
#endif
#define constant

typedef uint8_t uchar8[8];
typedef uint8_t uchar16[16];
typedef uint16_t ushort4[4];
typedef uint16_t ushort8[8];
typedef uint32_t uint2[2];
typedef uint32_t uint4[4];
typedef uint64_t ulong2[2];
typedef float float2[2];
typedef float float4[4];
typedef double double2[2];

static uint8_t clz64(uint64_t x)
{
	return x ? __builtin_clzl(x) : 64;
}

static uint8_t clz32(uint32_t x)
{
	return x ? __builtin_clz(x) : 32;
}

static uint8_t ctz32(uint32_t x)
{
	return x ? __builtin_ctz(x) : 32;
}
#else
typedef unsigned long uint64_t;
typedef signed long int64_t;
typedef unsigned int uint32_t;
typedef signed int int32_t;
typedef unsigned short uint16_t;
typedef signed short int16_t;
typedef unsigned char uint8_t;
typedef signed char int8_t;

#define clz64(x) clz((uint64_t)(x))
#define clz32(x) clz((uint32_t)(x))
static uint8_t ctz32(uint32_t x)
{
        return x ? __builtin_ctz(x) : 32;
}
#define memset(p, v, s) __builtin_memset(p, v, s)
#define assert(x) true
static bool isprint(char c)
{
	return c >= 0x20 && c <= 0x7e;
}
#endif

#ifdef __OPENCL_VERSION
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#endif
